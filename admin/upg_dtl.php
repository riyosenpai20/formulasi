<?php
    error_reporting(0);
    session_start();
    require '../../fungsi.php';
    require '../../configdb.php';
    $db  = cfg_sqli();
    $koneksi = cfg_pdo::connect();
    /**
        * Script:    DataTables server-side script for PHP 5.2+ and MySQL 4.1+
        * Notes:     Based on a script by Allan Jardine that used the old PHP mysql_* functions.
        *            Rewritten to use the newer object oriented mysqli extension.
        * Copyright: 2010 - Allan Jardine (original script)
        *            2012 - Kari Söderholm, aka Haprog (updates)
        * License:   GPL v2 or BSD (3-point)
    */
    mb_internal_encoding('UTF-8');

    //Ambil bagian di tbl_user
    $getbag = $koneksi->query("SELECT * FROM tbl_user WHERE NIK='$nik'");
    $rowbag = $getbag->fetch(PDO::FETCH_ASSOC);
    $bagian = $rowbag['bagian'];

    /**
        * Array of database columns which should be read and sent back to DataTables. Use a space where
        * you want to insert a non-database field (for example a counter or static image)
    */
    $aColumns = array("tbl_formulasi.NmJd", "tbl_formulasi.warna", "tbl_formulasi.nama", "tbl_formulasi.tanggal", "tbl_formulasi.NoUrt", "tbl_formulasi.tglTrial", "tbl_formulasi.tglUpg", "tbl_formulasi.val", "tbl_formulasi.KodeForm", "CONCAT(tbl_formulasi.nama,'-',tbl_formulasi.`NoUrt`) as jng", "tbl_formulasi.parent"); //Kolom Pada Tabel

    $aColumns2 = array("tbl_formulasi.NmJd", "tbl_formulasi.warna", "tbl_formulasi.nama", "tbl_formulasi.tanggal", "tbl_formulasi.NoUrt", "tbl_formulasi.tglTrial", "tbl_formulasi.tglUpg", "tbl_formulasi.val", "tbl_formulasi.KodeForm", "CONCAT(tbl_formulasi.nama,'-',tbl_formulasi.`NoUrt`) as jng", "tbl_formulasi.parent"); //Kolom Pada Tabel

    // Indexed column (used for fast and accurate table cardinality)
    $sIndexColumn = 'tbl_formulasi.id';

    // DB table to use
    $sTable = "tbl_formulasi"; // Nama Tabel

    // Input method (use $_GET, $_POST or $_REQUEST)
    $input =& $_POST;


    /**
            * Paging
    */
    $sLimit = "";
    if ( isset( $input['iDisplayStart'] ) && $input['iDisplayLength'] != '-1' ) {
            $sLimit = " LIMIT ".intval( $input['iDisplayStart'] ).", ".intval( $input['iDisplayLength'] );
    }

    /**
            * Ordering
    */
    $aOrderingRules = array();
    if ( isset( $input['iSortCol_0'] ) ) {
            $iSortingCols = intval( $input['iSortingCols'] );
            for ( $i=0 ; $i<$iSortingCols ; $i++ ) {
                    if ( $input[ 'bSortable_'.intval($input['iSortCol_'.$i]) ] == 'true' ) {
                            $aOrderingRules[] =
            "".$aColumns2[ intval( $input['iSortCol_'.$i] ) ]." "
            .($input['sSortDir_'.$i]==='asc' ? 'asc' : 'desc');
                    }
            }
    }

    if (!empty($aOrderingRules)) {
            $sOrder = " ORDER BY ".implode(", ", $aOrderingRules);
            } else {
            $sOrder = "";
    }

    /**
        * Filtering
        * NOTE this does not match the built-in DataTables filtering which does it
        * word by word on any field. It's possible to do here, but concerned about efficiency
        * on very large tables, and MySQL's regex functionality is very limited
    */
    $iColumnCount = 10;

    if ( isset($input['sSearch']) && $input['sSearch'] != "" ) {
            $aFilteringRules = array();
            for ( $i=0 ; $i<$iColumnCount ; $i++ ) {
                    if ( isset($input['bSearchable_'.$i]) && $input['bSearchable_'.$i] == 'true' ) {
                            $aFilteringRules[] = "".$aColumns2[$i]." LIKE '%".$db->real_escape_string( $input['sSearch'] )."%'";
                    }
            }
            if (!empty($aFilteringRules)) {
                    $aFilteringRules = array('('.implode(" OR ", $aFilteringRules).')');
            }
    }

    // Individual column filtering
    for ( $i=0 ; $i<$iColumnCount ; $i++ ) {
            if ( isset($input['bSearchable_'.$i]) && $input['bSearchable_'.$i] == 'true' && $input['sSearch_'.$i] != '' ) {
                    $aFilteringRules[] = "".$aColumns2[$i]." LIKE '%".$db->real_escape_string($input['sSearch_'.$i])."%'";
            }
    }

    if (!empty($input['sSearch'])) {
		$sWhere = " WHERE tbl_formulasi.NmJd is not null group by tbl_formulasi.NmJd";
            } else {
		$sWhere = " where tbl_formulasi.NmJd is not null group by tbl_formulasi.NmJd";
    }

    /**
            * SQL queries
            * Get data to display
    */
    $aQueryColumns = array();
    foreach ($aColumns as $col) {
            if ($col != ' ') {
                    $aQueryColumns[] = $col;
            }
    }

    $sQuery = "
    SELECT SQL_CALC_FOUND_ROWS ".implode(", ", $aQueryColumns)."
    FROM ".$sTable."".$sWhere.$sOrder.$sLimit;

    $rResult = $db->query( $sQuery ) or die($db->error);

    // Data set length after filtering
    $sQuery = "SELECT FOUND_ROWS()";
    $rResultFilterTotal = $db->query( $sQuery ) or die($db->error);
    list($iFilteredTotal) = $rResultFilterTotal->fetch_row();

    // Total data set length
    $sQuery = "SELECT COUNT(".$sIndexColumn.") FROM ".$sTable;
    $rResultTotal = $db->query( $sQuery ) or die($db->error);
    list($iTotal) = $rResultTotal->fetch_row();

/**
            * Output
    */
    $output = array(
        "sEcho"                => intval($input['sEcho']),
        "iTotalRecords"        => $iTotal,
        "iTotalDisplayRecords" => $iFilteredTotal,
        "aaData"               => array()
    );

    // Looping Data
        
    while ( $aRow = $rResult->fetch_assoc() ) {
        $nmLink = "<a href='../../form/dir/form_form.php?act=view&nof=".$aRow['KodeForm']."'>".$aRow['NmJd']."</a>";
        
        $ki = $aRow['parent'];
        $qrya = $koneksi->query("select KodeItem FROM tbl_troub WHERE KodeItem='$ki' && sts is not null");
        $hita = $qrya->rowCount();
        
        $qryb = $koneksi->query("select KodeItem from tbl_dtkaret WHERE `KodeItem`='$ki'");
        $hitb = $qryb->rowCount();
        $ker = ($hita/$hitb)*100;
        
        $row = array($nmLink, $aRow['warna'], $aRow['jng'], $ker."%");
        $output['aaData'][] = $row;
    }

    echo json_encode( $output );