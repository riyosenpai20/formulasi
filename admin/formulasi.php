<?php
session_start();
error_reporting(1);
date_timezone_set("Asia/Jakarta");
include '../../fungsi.php';
require '../../configdb.php';
$mpi = cfg_pdo::connect();
$lite = cfg_lite::sambung("../../../");
setlocale(LC_MONETARY, "id_ID");

$exe = empty($_GET['exe'])?NULL:$_GET['exe'];
$obj = empty($_GET['obj'])?NULL:$_GET['obj'];
$ide = empty($_GET['ide'])?NULL:$_GET['ide'];

$tglNow = date("Y-m-d");
$jamNow = date("H:i:s");
$strBln = $_GET['strBln'];
$strThn = $_GET['strThn'];
$PageNo = $_GET['PageNo'];

if($exe == "sv" && $obj == "head"):
    $nof = $_POST['nof'];
    $nm = strtoupper($_POST['nm']);
    $cb = strtoupper($_POST['cb']);
    $wr = $_POST['wr'];

    $qrya = $mpi->query("select formulasi FROM tbl_prm");
    $arra = $qrya->fetch(PDO::FETCH_OBJ);
    $fon = $arra->formulasi;

    $qryb = $mpi->query("select NoUrt FROM tbl_formulasi WHERE karet='$nm' && exp='$cb' && warna='$wr' && `stsDel`='F' ORDER BY id DESC limit 0,1");
    $arrb = $qryb->fetch(PDO::FETCH_OBJ);
    $nou = $arrb->NoUrt;

    if(empty($nou)):
        $NoUrut = 1;
    else:
        $NoUrut = $nou+1;
    endif;

    if($nof != $fon):

        $qry1 = $mpi->prepare("update tbl_prm SET formulasi=?");
        $exe1 = $qry1->execute(array($nof));

        $qry2 = $mpi->prepare("insert INTO tbl_formulasi (`KodeForm`, karet, exp, warna, nama, NoUrt, tanggal) VALUES (?, ?, ?, ?, ?, ?, ?)");
        $exe2 = $qry2->execute(array($nof, $nm, $cb, $wr, $nm." ".$cb, $NoUrut, $tglNow));

        echo "0|Data disimpan";
    else:
        echo "1|Data sudah tersimpan!!";
    endif;
elseif($exe == "load" && $obj == "dtl"):
    $nof = $_GET['nof'];
    $qryb = $mpi->query("select a.*, b.nama FROM tbl_formulasidtl as a left join tbl_daftar_item as b on a.bahan=b.`KodeItem` WHERE `KodeForm`='$nof' ORDER BY `NoUrt` ASC");
    $arrb = $qryb->fetchAll(PDO::FETCH_OBJ);
    ?>
    </br>
    <div class="col-md-12">
        <div class="panel panel-info">
            <div class="panel-heading"></div>
            <div class="panel-body">
                <table class="table table-bordered table-hover table-condensed table-stripped">
                    <thead>
                        <tr>
                            <th width="2%">No.</th>
                            <th>Nama</th>
                            <th width="20%">Phr</th>
                            <th width="20%">Kantong</th>
                            <th>@Harga (kg)</th>
                            <th>Harga Total</th>
                            <th width="5%">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $no = 1;
                        foreach ($arrb as $b):
                            $ki = $b->bahan;
                            $noid = $b->id;
                            $ktg = $b->kantong;
                            $sen = $b->pers;
                            $qryc = $mpi->query("select haru FROM tbl_daftar_item WHERE `KodeItem`='$ki'");
                            $arrc = $qryc->fetch(PDO::FETCH_OBJ);
                            $harsat = $arrc->haru;
                            $hartot = $b->phr * $harsat;
                            $x[] = $b->phr;
                            $y[] = $hartot;
                            ?>
                            <tr>
                                <td><?php echo $no; ?></td>
                                <td><?php echo $b->nama; ?></td>
                                <td><input name="phr" id="phr<?php echo $nof.'|'.$ki; ?>" type="text" onkeypress="chg(this.id,this.value,event)" value="<?php echo $b->phr; ?>" class="form-control input-sm"/></td>
                                <td>
                                    <div class="col-md-12">
                                    <?php
                                        $kan = explode(",", $ktg);
                                        $per = explode(",", $sen);
                                        if(empty($ktg)):
                                            ?>
                                        <div class="col-md-8">
                                            <select class="form-control input-sm col-md-8" onchange="knt(this.id,this.value)" name="knt" id="<?php echo $noid; ?>">
                                                <?php
                                                for($k=1;$k<=10;$k++):
                                                ?>
                                                <option value="<?php echo $k; ?>" <?php echo $k==$ktg?"selected":NULL; ?>><?php echo $k; ?></option>
                                                <?php
                                                endfor;
                                                ?>
                                            </select>
                                        </div>
                                            <?php
                                        else:
                                            if(count($kan) > 1):
                                                for($q = 0;$q < count($kan); $q++):
                                                    echo $kan[$q].":".$per[$q]."; ";
                                                endfor;
                                            else:
                                                ?>
                                        <div class="col-md-8">
                                                <select  class="form-control input-sm col-md-8" onchange="knt(this.id,this.value)" name="knt" id="<?php echo $noid; ?>">
                                                    <?php
                                                    for($l=0;$l<=10;$l++):
                                                    ?>
                                                    <option value="<?php echo $l; ?>"<?php echo $l==$ktg?"selected":NULL; ?>><?php echo $l; ?></option>
                                                    <?php
                                                    endfor;
                                                    ?>
                                                </select>
                                        </div>
                                                <?php
                                            endif;
                                        endif;
                                    ?>
                                        <div class="col-md-4">
                                    <a class="btn btn-primary btn-xs" href="#pop" id="ktg<?php echo $noid; ?>" data-toggle="modal" onclick="ktg(this.id)"><i class="fa fa-cubes"></i></a>
                                        </div>
                                    </div>
                                </td>
                                <td><?php number($harsat,2,",","."); ?></td>
                                <td><?php echo number(round($hartot,2),2,",","."); ?></td>
                                <td>
                                    <a class="btn btn-warning btn-xs" id="edt<?php echo $nof."|".$ki; ?>" onclick="rbh(this.id)"><i class="fa fa-pencil"></i></a>
                                    <a class="btn btn-danger btn-xs" id="rmv<?php echo $nof."|".$ki; ?>" onclick="hps(this.id)"><i class="fa fa-remove"></i></a>
                                </td>
                            </tr>
                            <?php
                            $no++;
                        endforeach;
                        ?>
                            <tr>
                                <td colspan="5" class="text-right"><strong>Jumlah Harga Total</strong></td>
                                <td><?php echo number(round(array_sum($y),2),2,",","."); ?></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td colspan="2" class="text-right"><strong>Jumlah PHR</strong></td>
                                <td><?php echo number(array_sum($x),2,",","."); ?></td>
                                <td></td>
                                <td class="text-right"><strong>Biaya per phr</strong></td>
                                <td><?php echo number(round((array_sum($y)/  array_sum($x)),2),2,",","."); ?></td>
                                <td></td>
                            </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <?php
elseif($exe == "smpn" && $obj == "dtl"):
    $ki = $_POST['KodeItem'];
    $nof = $_POST['nof'];

    $qryd = $mpi->query("select * FROM tbl_formulasidtl WHERE `KodeForm`='$nof' && bahan='$ki'");
    $cntd = $qryd->rowCount();

    $qryf = $mpi->query("select b.Harsat, c.TglPO FROM tbl_daftar_item as a left join tbl_po_dtl as b on a.id=b.`NmBrg` left join tbl_po as c on b.`NoPO`=c.`NoPO` WHERE a.`KodeItem`='$ki' order by c.`TglPO` DESC limit 0,1");
    $arrf = $qryf->fetch(PDO::FETCH_OBJ);
    $harsat = $arrf->Harsat;

    if($cntd > 0):
        echo "0|Bahan baku telah ada pada formulasi";
    else:
        $qrye = $mpi->query("select `NoUrt` FROM tbl_formulasidtl WHERE `KodeForm`='$nof' ORDER BY id DESC limit 0,1");
        $arre = $qrye->fetch(PDO::FETCH_OBJ);

        if(!empty($arre->NoUrt)):
            $hit = $arre->NoUrt+1;
        else:
            $hit = 1;
        endif;

        $qry1 = $mpi->prepare("insert INTO tbl_formulasidtl (`KodeForm`, bahan, `NoUrt`, tgl, harsat) VALUES (?, ?, ?, ?, ?)");
        $qry1->execute(array($nof, $ki, $hit, $tglNow." ".$jamNow, $harsat));

        echo "1|Bahan baku berhasil disimpan";
    endif;

elseif($exe == "upd" && $obj == "dtl"):
    $ki = $_POST['ki'];
    $nof = $_POST['nof'];
    $lue = $_POST['lue'];

    $qry1 = $mpi->prepare("update tbl_formulasidtl SET phr=? WHERE `KodeForm`=? && bahan=?");
    $qry1->execute(array($lue, $nof, $ki));

elseif($exe == "dlt" && $obj == "head"):
    $qry1 = $mpi->prepare("update tbl_formulasi SET `stsDel`=? WHERE `KodeForm`=?");
    $qry1->execute(array("T", $ide));

    header("location:../../list/dir/list_formulasi.php");
elseif($exe == "sv" && $obj == "dup"):
    $nof = $_POST['nof'];
    $nm = strtoupper($_POST['nm']);
    $cb = strtoupper($_POST['cb']);
    $wr = $_POST['wr'];
    $nama = $nm." ".$cb;

    $qrya = $mpi->query("select NoUrt FROM tbl_formulasi WHERE nama='$nama' && warna='$wr' && `stsDel`='F' ORDER BY id DESC limit 0,1");
    $arra = $qrya->fetch(PDO::FETCH_OBJ);
    $nou = $arra->NoUrt;

    if(empty($nou)):
        $hit = 1;
    else:
        $hit = $nou+1;
    endif;

    $qry1 = $mpi->prepare("update tbl_formulasi SET karet=?, nama=?, exp=?, `NoUrt`=?, warna=? WHERE `KodeForm`=?");
    $qry1->execute(array($nm, $nm." ".$cb, $cb, $hit, $wr, $nof));

    echo "Nama telah disimpan!";
elseif($exe == "rmv" && $obj == "dtl"):
    $nof = $_POST['nof'];
    $ki = $_POST['ki'];

    $qrya = $mpi->query("select bahan FROM tbl_formulasidtl WHERE `KodeForm`='$nof' && bahan='$ki'");
    $hita = $qrya->rowCount();

    if($hita > 0):
        $qry1 = $mpi->prepare("delete FROM tbl_formulasidtl WHERE `KodeForm`=? && bahan=?");
        $qry1->execute(array($nof, $ki));

        echo "0|Hapus data berhasil!";
    else:
        echo "1|Hapus data Gagal, karena data yang ingin anda hapus tidak ditemukan.";
    endif;
elseif($exe == "hd" && $obj == "on"):
    $nof = $_POST['nof'];
    $nm = strtoupper($_POST['nm']);
    $tp = $_POST['tp'];

    $qrya = $mpi->query("select addon FROM tbl_prm");
    $arra = $qrya->fetch(PDO::FETCH_OBJ);
    $fon = $arra->addon;

    if($nof != $fon):
        $qry1 = $mpi->prepare("update tbl_prm SET addon=?");
        $qry1->execute(array($nof));

        $qry2 = $mpi->prepare("insert INTO tbl_addon (`KodeAddon`, `type`, nama, tgl) VALUES (?, ?, ?, ?)");
        $qry2->execute(array($nof, $tp, $nm, $tglNow));
        echo "0|Data berhasil disimpan!";
    else:
        echo "1|Data telah tersimpan!";
    endif;
elseif($exe == "dt" && $obj == "on"):
    $nof = $_POST['nof'];
    $ki = $_POST['KodeItem'];

    $qryd = $mpi->query("select * FROM tbl_addondtl WHERE `KodeAddon`='$nof' && bahan='$ki'");
    $cntd = $qryd->rowCount();

    if($cntd > 0):
        echo "0|Bahan baku telah ada pada formulasi";
    else:
        $qrye = $mpi->query("select `NoUrt` FROM tbl_addondtl WHERE `KodeAddon`='$nof' ORDER BY id DESC limit 0,1");
        $arre = $qrye->fetch(PDO::FETCH_OBJ);

        if(!empty($arre->NoUrt)):
            $hit = $arre->NoUrt+1;
        else:
            $hit = 1;
        endif;

        $qry1 = $mpi->prepare("insert INTO tbl_addondtl (`KodeAddon`, bahan, `NoUrt`, tgl) VALUES (?, ?, ?, ?)");
        $qry1->execute(array($nof, $ki, $hit, $tglNow." ".$jamNow));

        echo "1|Bahan baku berhasil disimpan";
    endif;
elseif($exe == "upd" && $obj == "ondtl"):
    $ki = $_POST['ki'];
    $nof = $_POST['nof'];
    $lue = $_POST['lue'];

    $qry1 = $mpi->prepare("update tbl_addondtl SET phr=? WHERE `KodeAddon`=? && bahan=?");
    $qry1->execute(array($lue, $nof, $ki));
elseif($exe == "rmv" && $obj == "ondtl"):
    $nof = $_POST['nof'];
    $ki = $_POST['ki'];

    $qrya = $mpi->query("select bahan FROM tbl_addondtl WHERE `KodeAddon`='$nof' && bahan='$ki'");
    $hita = $qrya->rowCount();

    if($hita > 0):
        $qry1 = $mpi->prepare("delete FROM tbl_addondtl WHERE `KodeAddon`=? && bahan=?");
        $qry1->execute(array($nof, $ki));

        echo "0|Hapus data berhasil!";
    else:
        echo "1|Hapus data Gagal, karena data yang ingin anda hapus tidak ditemukan.";
    endif;
elseif($exe == "trial" && $obj == "form"):
    $nof = $_POST['nof'];

    ?>
    <script>
        function addMore(){
            url = "http://<?php echo $_SERVER['SERVER_NAME']; ?>/project1_5/custompage/dir/formulasi.php?exe=load&obj=trl";
            $("<tr>").load(url, function (){
                $("#trl").append($(this).html());
            });
        }
        function deltRow(){
            $("tr.isRow").each(function (index, item){
                jQuery(":checkbox", this).each(function (){
                    if($(this).is(":checked")){
                        $(item).remove();
                    }
                });
            });
        }
    </script>
    <input type="hidden" value="try" name="knc" id="knc"/>
    <div class="col-md-12 form-horizontal" role="form">
        <div>
            <label>Qty</label>
            <div class="input-group">
                <input type="text" class="form-control" id="quan"/>
                <span class="input-group-addon">
                    <span><i class="fa fa-cubes"></i></span>
                </span>
            </div>
            <br>
        </div>
        <table width='900' border='0' cellpadding='3' cellspacing='1' class="table table-bordered table-condensed">
            <thead>
                <tr>
                    <td width="5%"><b>Chk</b></td>
                    <td><b>Nama Add-On</b></td>
                </tr>
            </thead>
            <tbody id="trl">

            </tbody>
        </table>
        <a class="btn btn-default btn-xs" onclick="addMore()"><i class="fa fa-plus"></i></a>
        <a class="btn btn-danger btn-xs" onclick="deltRow()"><i class="fa fa-minus"></i></a>
    </div>
    <?php
elseif($exe == "sv" && $obj == "hsl"):
    $nof = $_POST['nof'];
    $hst1 = $_POST['hst1'];
    $hst2 = $_POST['hst2'];
    $hst3 = $_POST['hst3'];
    $hsr1 = $_POST['hsr1'];
    $hsr2 = $_POST['hsr2'];
    $hsr3 = $_POST['hsr3'];
    $tns = $_POST['tensile'];
    $abbr = $_POST['abbr'];
    $hsl = $_POST['hsl'];

    $qrya = $mpi->prepare("update tbl_formulasi SET hst1=?, hst2=?, hst3=?, hsr1=?, hsr2=?, hsr3=?, `Tensile`=?, abrasion=?, hasil=?, tglRnD=? WHERE `KodeForm`=?");
    $qrya->execute(array($hst1, $hsr2, $hst3, $hsr1, $hsr2, $hsr3, $tns, $abbr, $hsl, $tglNow, $nof));
    
    header("location:../../form/dir/form_form.php?act=rnd&nof=$nof");
elseif($exe == "sv" && $obj == "ket"):
    $nof = $_POST['nof'];
    $isi = $_POST['isi'];

    $qrya = $mpi->prepare("update tbl_formulasi SET keterangan=? WHERE `KodeForm`=?");
    $qrya->execute(array($isi, $nof));
elseif($exe == "load" && $obj == "cng"):
    $kd = $_GET['kd'];
    $qrya = $mpi->query("select a.`KodeForm`, b.nama, b.warna, b.`NoUrt`, a.`NoUrt` as urt, b.`NmJd` FROM tbl_formulasidtl as a left join tbl_formulasi as b on a.`KodeForm`=b.`KodeForm` WHERE a.bahan='$kd'");
    $arra = $qrya->fetchAll(PDO::FETCH_OBJ);

    ?>
    <div class="panel panel-info">
        <div class="panel-heading"></div>
        <div class="panel-body">
            <table class="table table-bordered table-hover table-condensed table-stripped">
                <thead>
                    <tr>
                        <th>opt</th>
                        <th>Nama</th>
                        <th>Warna</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    foreach ($arra as $a):
                        $kf = $a->KodeForm;
                        $nm = $a->nama;
                        $nu = $a->NoUrt;
                        $wr = $a->warna;
                        $ur = $a->urt;
                        $nj = $a->NmJd;

                        ?>
                        <tr>
                            <td><input name="chk[]" type="checkbox" id="chk" value="<?php echo $kf; ?>"/></td>
                            <td><?php echo $nm."-".$nu." (Nama Jadi : ".$nj.")"; ?></td>
                            <td><?php echo $wr; ?></td>
                        </tr>
                        <?php
                    endforeach;
                    ?>
                </tbody>
            </table>
        </div>
    </div>
    <?php
elseif($exe == "cng" && $obj == "aks"):
    $kd = $_POST['kd'];
    $isi = $_POST['isi'];
    $awl = $_POST['awl'];

    $kdb = explode(",", $kd);
    for($a=0;$a<count($kdb);$a++):
        $qrya = $mpi->prepare("update tbl_formulasidtl SET bahan=? WHERE `KodeForm`=? && bahan=?");
        $qrya->execute(array($isi, $kdb[$a], $awl));
    endfor;
elseif($exe == "pop" && $obj == "namjad"):
    $nof = $_POST['nof'];

    $qrya = $mpi->query("select nama, NoUrt, warna, karet, exp from tbl_formulasi where KodeForm='$nof'");
    $arra = $qrya->fetch(PDO::FETCH_OBJ);
    $nama = $arra->nama;
    $warna = $arra->warna;
    $urt = $arra->NoUrt;

    $qryk = $mpi->query("select `KodeItem`, nama, warna, type, `SaldoAkir` FROM tbl_daftar_item WHERE `stsDel`='F' && typeItem='1' ORDER BY nama ASC");
    $arrk = $qryk->fetchAll(PDO::FETCH_OBJ);
    ?>
    <script>
    var satad = <?php
        echo "[";
            foreach($arrk as $blk):
                $ki = $blk->KodeItem;

                echo "{";
                    echo "jng: '".trim($blk->nama)."',";
                    echo "wrn: '".$blk->warna."',";
                    echo "kde: '".$blk->KodeItem."',";
                    echo "tpe: '".$blk->type."'";
                echo "},";
            endforeach;
                echo "{";
                    echo "jng: '',";
                    echo "wrn: '',";
                    echo "kde: '',";
                    echo "tpe: ''";

                echo "}";
                echo "]";
        ?>;
        $.each(satad, function (i, line){
           line.value = line.jng;
       });
        $('#namjad').autocomplete({
            minLength: 1,
            source: satad,
            focus: function (event, ui){
                $('#namjad').val(ui.item.jng);
                return false;
            },
            select: function (event, ui){
                $('#namjad').val(ui.item.kde);
                return  false;
            }
        }).data("ui-autocomplete")._renderItem = function (ul, item){
            return $("<li>").append("<a>" + item.jng + " " + item.wrn + "</a>")
                    .appendTo(ul);
                };
                $("#namjad").autocomplete("option", "appendTo", "#eventInsForm");
                </script>

                <input type="hidden" name="knc" id="knc" value="namjad"/>
    <div class="panel panel-primary">
        <div class="panel-heading">Anda akan meng-Upgrade <?php echo $nama."-".$urt." ".$warna; ?></div>
        <div class="panel-body">
            <form id="eventInsForm">
                <table class="table table-bordered">
                    <tr>Nama Jadi :</tr>
                    <tr><input type="text" name="namjad" class="form-control" id="namjad"/></tr>
                </table>
            </form>
        </div>
    </div>
    <?php
elseif($exe == "sv" && $obj == "namjad"):
    $nof = $_POST['nof'];
    $kar = $_POST['kar'];
    $namjad = strtoupper($_POST['namjad']);
    $qrya = $mpi->query("select nama, NoUrt, warna, karet, exp, hst1, hst2, hst3 from tbl_formulasi where KodeForm='$nof'");
    $arra = $qrya->fetch(PDO::FETCH_OBJ);
    $nama = $arra->nama;
    $warna = $arra->warna;
    $hst1 = $arra->hst1;
    $hst2 = $arra->hst2;
    $hst3 = $arra->hst3;
    $hst = round(($hst1 + $hst2 + $hst3)/3);
    $cbJd = trim(str_replace($kar, "", $namjad));

    $qryc = $mpi->query("select nama FROM tbl_daftar_item WHERE KodeItem='$namjad'");
    $hitc = $qryc->rowCount();
    $arrc = $qryc->fetch(PDO::FETCH_OBJ);
    $namc = $arrc->nama;

    if($hitc > 0):
        $qry1 = $mpi->prepare("update tbl_formulasi SET `NmJd`=?, parent=?, `tglUpg`=?, sts=?, expJd=? WHERE `KodeForm`=?");
        $qry1->execute(array($namc, $namjad, $tglNow, 2, $cbJd, $nof));

        $qry2 = $mpi->prepare("update tbl_daftar_item SET HS=?, frm=? WHERE `KodeItem`=?");
        $qry2->execute(array($hst, $nof, $namjad));
    else:
        $qryd = $mpi->query("select `NoBrg` FROM tbl_kat WHERE id_kat=1");
        $arrd = $qryd->fetch(PDO::FETCH_OBJ);
        $nos = $arrd->NoBrg+1;

        $qrye = $mpi->query("select `NoUrut` FROM tbl_daftar_item WHERE `typeItem`=1 ORDER BY `NoUrut` DESC limit 0,1");
        $arre = $qrye->fetch(PDO::FETCH_OBJ);
        $nour = $arre->NoUrut+1;

        $ki = str_replace(" ", "", ("BK-KR-".substr($namjad, 0, 4)."-".$nos));

        $qry3 = $mpi->prepare("insert INTO tbl_daftar_item (`KodeItem`, nama, `typeItem`, `tglRevisi`, satuan, `NoUrut`, warna, `type`, takar, HS, frm) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
        $qry3->execute(array($ki, $namjad, 1, $tglNow, "KG", $nour, $warna, "SOSG", 1, $hst, $nof));

        $qry4 = $mpi->prepare("update tbl_formulasi SET parent=?, sts=?, `tglUpg`=?, `NmJd`=?, val=?, expJd=? WHERE `KodeForm`=?");
        $qry4->execute(array($ki, 2, $tglNow, $namjad, 1, $cbJd, $nof));

        $qry5 = $mpi->prepare("update tbl_kat SET `NoBrg`=? WHERE id_kat=?");
        $qry5->execute(array($nos, 1));
    endif;
elseif($exe == "add" && $obj == "ele"):
    $nof = $_GET['nof'];
    $qry1 = $mpi->prepare("update tbl_addon SET `stsDel`=? WHERE `KodeAddon`=?");
    $qry1->execute(array("T", $nof));

    header("location:../../list/dir/list_addin.php?strBln=$strBln&strThn=$strThn&PageNo=$PageNo");
elseif($exe == "gnt" && $obj == "knt"):
    $aidi = $_POST['aidi'];
    $valiu = $_POST['valiu'];

    $qry1 = $mpi->prepare("update tbl_formulasidtl SET kantong=? WHERE id=?");
    $qry1->execute(array($valiu, $aidi));
elseif($exe == "load" && $obj == "trl"):
    ?>
        <tr class="isRow">
            <td align="center" class="labelform">
                <input name="slc[]" type="checkbox"/>
            </td>
            <td class="labelform">
                <select class="form-control" name="xtreme[]">
                    <?php
                    $qryg = $mpi->query("select nama, `KodeAddon` FROM tbl_addon ORDER BY nama ASC");
                    $arrg = $qryg->fetchAll(PDO::FETCH_OBJ);
                    foreach ($arrg as $g):
                    ?>
                    <option value="<?php echo $g->KodeAddon; ?>"><?php echo $g->nama; ?></option>
                    <?php
                    endforeach;
                    ?>
                </select>
            </td>
        </tr>
    <?php
elseif($exe == "try" && $obj == "inc"):
    $isi = $_POST['isi'];
    $nof = $_POST['nof'];
    $quan = $_POST['quan'];
    //qty digunakan untuk spk trial -->> setelah formulasi selesai

    for($x = 0; $x < count($isi); $x++):
        $qry1 = $mpi->prepare("insert INTO tbl_tryinc (`KodeForm`, `KodeAddon`) VALUES (?, ?)");
        $qry1->execute(array($nof, $isi[$x]));
    endfor;
    $qry2 = $mpi->prepare("update tbl_formulasi SET sts=?, `tglTrial`=?, `qtyTrl`=? WHERE `KodeForm`=?");
    $qry2->execute(array(1, $tglNow, $quan, $nof));
    
    $qryb = $lite->query("select reqtrial from tbl_prm where id='1'");
    $arrb = $qryb->fetchArray(SQLITE3_BOTH);
    $noReq = $arrb["reqtrial"];
    $bra = explode($noReq, "");
    
    $noru = str_pad($bra[0]+1, 4, "0", STR_PAD_LEFT);
    $no = $noru."/".$bra[1]."/".$bra[2];
    $qry1 = $lite->prepare("insert into tbl_reqtrial (Noreq, tgl, ket) values (?, ?, ?)");
    $qry1->execute(array($no, $tglNow, $ket));
    
    $qry2 = $lite->prepare("insert into tbl_reqtrialdtl (Noreq, NoForm, qty, ket, desk, addon) values (?, ?, ?, ?, ?)");
    $qry2->execute(array($no, $nof, $quan, $ket, $desc, implode($isi, ",")));

elseif($exe == "btn" && $obj == "ok"):
    $nof = $_POST['nof'];

    $qry1 = $mpi->prepare("update tbl_formulasi SET val=? WHERE `KodeForm`=?");
    $qry1->execute(array(1, $nof));
elseif($exe == "btn" && $obj == "nok"):
    $nof = $_POST['nof'];

    $qry1 = $mpi->prepare("update tbl_formulasi SET val=? WHERE `KodeForm`=?");
    $qry1->execute(array(2, $nof));
elseif($exe == "pop" && $obj == "ktg"):
    $nof = $_POST['nof'];
    $id = $_POST['id'];
    $qrya = $mpi->query("select kantong, pers FROM tbl_formulasidtl WHERE id='$id'");
    $arra = $qrya->fetch(PDO::FETCH_OBJ);
    $kantong = $arra->kantong;
    $persentase = $arra->pers;
    $tong = explode(",", $kantong);
    $pers = explode(",", $persentase);

    ?>
    <script>
        function addMore(){
            url = "http://<?php echo $_SERVER['SERVER_NAME']; ?>/project1_5/custompage/dir/formulasi.php?exe=load&obj=ktg";
            $("<tr>").load(url, function (){
                $("#trl").append($(this).html());
            });
        }
        function deltRow(){
            $("tr.isRow").each(function (index, item){
                jQuery(":checkbox", this).each(function (){
                    if($(this).is(":checked")){
                        $(item).remove();
                    }
                });
            });
        }
    </script>
    <input type="hidden" value="ktg" name="knc" id="knc"/>
    <input type="hidden" value="<?php echo $id; ?>" id="id"/>
    <div class="col-md-12 form-horizontal" role="form">
        <table width='900' border='0' cellpadding='3' cellspacing='1' class="table table-bordered table-condensed">
            <thead>
                <tr>
                    <td width="5%"><b>Chk</b></td>
                    <td><b>Kantong</b></td>
                    <td><b>% PHR</b></td>
                </tr>
            </thead>
            <tbody id="trl">
                <?php
                if(count($tong)>1):
                    for($h=0;$h<count($tong);$h++):
                    ?>
                <tr class="isRow">
                    <td align="center" class="labelform">
                        <input name="slc[]" type="checkbox"/>
                    </td>
                    <td class="labelform">
                        <select class="form-control" name="xtreme[]">
                        <?php
                        for($i=1;$i<=10;$i++):
                            ?>
                            <option value="<?php echo $i; ?>" <?php echo $i==$tong[$h]?"selected":NULL; ?>><?php echo $i; ?></option>
                            <?php
                        endfor;
                        ?>
                        </select>
                    </td>
                    <td class="labelform">
                        <?php
                            ?>
                            <input type="text" name="prktg[]" class="form-control" value="<?php echo $pers[$h]; ?>"/>
                            <?php
                        ?>
                    </td>
                </tr>
                    <?php
                    endfor;
                endif;
                ?>
            </tbody>
        </table>
        <a class="btn btn-default btn-xs" onclick="addMore()"><i class="fa fa-plus"></i></a>
        <a class="btn btn-danger btn-xs" onclick="deltRow()"><i class="fa fa-minus"></i></a>
    </div>
    <?php
elseif($exe == "load" && $obj == "ktg"):
    ?>
        <tr class="isRow">
            <td align="center" class="labelform">
                <input name="slc[]" type="checkbox"/>
            </td>
            <td class="labelform">
                <select class="form-control" name="xtreme[]">
                    <?php
                    for($g=1;$g<=10;$g++):
                        ?>
                            <option value="<?php echo $g; ?>"><?php echo $g; ?></option>
                        <?php
                    endfor;
                    ?>
                </select>
            </td>
            <td class="labelform">
                <input name="prktg[]" id="prktg" class="form-control" type="text"/>
            </td>
        </tr>
    <?php
elseif($exe == "sv" && $obj == "ktg"):
    $isi = implode(",", $_POST['isi']);
    $nof = $_POST['nof'];
    $prktg = implode(",", $_POST['prktg']);
    $id = $_POST['id'];
    $maxktg = sort(end($_POST['isi']));

    echo $isi." <br>".$prktg;

    $qry1 = $mpi->prepare("update tbl_formulasidtl SET kantong=?, pers=?, maxktg=? WHERE id=?");
    $qry1->execute(array($isi, $prktg, $maxktg,$id));
endif;
