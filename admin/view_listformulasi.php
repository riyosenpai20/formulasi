<?php
error_reporting(1);
session_start();

//require "../fungsi.php";
require '../configdb.php';

$mpi = cfg_pdo::connect();
date_default_timezone_set("Asia/Jakarta");

//Status Level
$strLevel = $_SESSION['level'];

//Status Access
$strAccess = $_SESSION['access'];

$username = $_SESSION['username2'];

//$qr = $mpi->query("select bagian, jabatan FROM tbl_kry_departemen WHERE aktif='T' && sts_job='T' && `NIK`='$strNIK'");
//$bgn = $qr->fetch(PDO::FETCH_OBJ);

$jabatan = $bgn->jabatan;
$bagian = $bgn->bagian;

$nama = $_GET['nama'];
$wrn = $_GET['wrn'];
$strBulan = "strBulan";
$strTahun = "strTahun";
$sts = $_GET['sts'];

//Set the page size
$PageSize = 10;
$StartRow = 0;
        

?>
<!DOCTYPE html>
<html>
    <head>
	<title>Daftar Formulasi</title>	
        <link rel="stylesheet" href="../addon/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="../addon/font-awesome-4.7.0/css/font-awesome.min.css"/>
        <link rel="stylesheet" type="text/css" href="../addon/datatables/dataTables.bootstrap.css">
        <link rel="stylesheet" type="text/css" href="../addon/datatables/dataTables.responsive.css">
        <script type="text/javascript" src="../addon/jquery/jquery-3.2.0.min.js"></script>
        <script type="text/javascript" src="../addon/bootstrap.min.js" ></script>
        <script type="text/javascript" src="../addon/datatables/jquery.dataTables.js"></script>
        <script type="text/javascript" src="../addon/datatables/dataTables.responsive.js"></script>
        <script type="text/javascript" src="../addon/datatables/dataTables.bootstrap.js"></script>
        <script type="text/javascript" src="../addon/datatables/common.js"></script>
        <SCRIPT type="text/javascript" src="formulasi.js"></script>
                
        <script type="text/javascript" >

            var dTable;
            // #Example adalah id pada table

            $(document).ready(function() {
                    //Onclick

                dTable = $('#exa').DataTable( {
                    "bProcessing": true,
                    "bServerSide": true,
                    "bJQueryUI": false,
                    "responsive": false,
                    "sAjaxSource": "formulasi_dtl.php?nama=<?php echo $nama; ?>&wrn=<?php echo $wrn; ?>&sts=<?php echo $sts; ?>", // Load Data
                    "sServerMethod": "POST",
                    "order": [[ 0, "asc" ]],
                    "deferRender": true,
                    "columnDefs": [
                        {"targets": 0,"orderable": true},
                        {"targets": 1,"orderable": true}
                    ],
                    stateSave: true
                } );

            } );

        </script>                
    </head>
    <?php
	date_default_timezone_set('Asia/Jakarta');
    ?>
    <body style="font-size:12px">
        <div align="left" id="content">
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-form" role="search">
                <div class="form-group">
                    <form action="<?php echo $_SERVER['SCRIPT_NAME'];?>" method="get" >
                        <table width="100%" border="0" cellspacing="0" cellpadding="3">
                                <tr>
                                    <td width="3%"><a class="btn btn-default btn-xs" href="../../list/dir/list_formulasi.php"><i class="fa fa-home"></i></a>
                                            <select class="form-control" name="sts">
                                                <option <?php echo $sts == 1?"selected":NULL; ?> value="1">All</option>
                                                <option <?php echo $sts == 2?"selected":NULL; ?> value="2">Trial</option>
                                                <option <?php echo $sts == 3?"selected":NULL; ?> value="3">Upgrade</option>
                                                <option <?php echo $sts == 4?"selected":NULL; ?> value="4">Not OK</option>
                                                <option <?php echo $sts == 5?"selected":NULL; ?> value="5">OK</option>
                                            </select>
                                            <input type="hidden" name="nama" value="<?php echo $nama; ?>"/>
                                            <input type="hidden" name="wrn" value="<?php echo $wrn; ?>"/>
                                            &nbsp;&nbsp;
                                            &nbsp;
                                            <button name="submit" title="Cari" type="submit" class="btn btn-success" id="Submit" value=""><i class="glyphicon glyphicon-search"></i></button>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="btn btn-info" title="Formula Baru" href="form_form.php?act=new&strThn=<?php echo $strThn; ?>&strBln=<?php echo $strBln; ?>"><i class="glyphicon glyphicon-plus"></i></a>
                                        </td>
                                </tr>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </nav>
        </DIV>
<br/>
<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel" id="pop">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div class="close" type="button" data-dismiss="modal"><span>&times;</span></div>
		<h3 id="kepala"></h3>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
		<button type="button" class="btn btn-success" id="smpn"><span>Submit</span></button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel" id="up">
</div>

<div class="panel panel-info">
    <div class="panel-heading" align="center"><b></b></div>
    <div class="panel-body">
        <form name="frm_accfpb" method="post" action="act_ccr.php?strBln=<?php echo $strBln; ?>&tion=chk&strThn=<?php echo $strThn; ?>&PageNo=<?php echo $PageNo; ?>&keyword=<?php echo $keyword; ?>">
                <div class="panel panel-default">
                    <DIV class="panel-body">
                        <table id="exa"  cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered table-condensed table-hover" width="100%" style="font-size:12px">
                            <thead>
                                <tr>
                                    <th colspan="10"><center>List Formulasi <?php echo $nama; ?> <?php echo $wrn; ?></center></th>
                                </tr>
                                <tr>
                                    <th width="2%"><center>Nama</center></th>
                                    <th width="4%" class="text-center">Nama Jadi</th>
                                    <th width="4%"><center>Warna</center></th>
                                    <th width="2%"><center>Tanggal</center></th>
                                    <th width="7%"><center>Status</center></th>
                                    <th width="5%"><center>Act</center></th>
                                    <TH width="5%"><center>Kerusakan</center></TH>
                                </tr>
                            </thead>
                            <tbody>
                                
                            </tbody>
                            <tfoot>

                            </tfoot>
                        </table>
                    </DIV>
                </DIV>
        </form>
            </div>
        </div>
    </body>
</html>