<?php
session_start();
error_reporting(1);
include '../../configdb.php';
include '../../fungsi.php';
$mpi = cfg_pdo::connect();

$now = date("Y-m-d");
$strThn = isset($_GET['strThn']) ? $_GET['strThn'] : date("Y");

$strNIK = $_SESSION['NIK'];
$dif = $mpi->query("select bagian, jabatan FROM tbl_kry_departemen WHERE aktif='T' && sts_job='T' && `NIK`='$strNIK'")
    ->fetch(PDO::FETCH_OBJ);

$bagian = $dif->bagian;
$jabatan = $dif->jabatan;
?>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="../../bootstrap/bootstrap-4.1.3/dist/css/bootstrap.min.css"/>
        <link rel="stylesheet" type="text/css" href="../../script/assets/bootstrap/dataTables.bootstrap4.min.css"/>
        <link rel="stylesheet" type="text/css" href="../../add-in/font-awesome-4.7.0/css/font-awesome.min.css">
        <script src="../../jquery/jquery-3.3.1.js" type="text/javascript"></script>
        <script src="../../script/assets/bootstrap/jquery.dataTables.min.js" type="text/javascript"></script>
        <script src="../../bootstrap/bootstrap-4.1.3/dist/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="../../script/assets/bootstrap/dataTables.bootstrap4.min.js" type="text/javascript"></script>
        <script src="../../node_modules/vue/dist/vue.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="../../customjs/dir/appjdw.vue.js"></script>
    </head>
    <body style="font-size:12px;">
        <div>
            <nav class="navbar navbar-dark bg-primary navbar-expand-lg fixed-top">
                <a class="navbar-brand" href="#"><i class="fa fa-home"></i></a>

                <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
                    <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                    </ul>
                    <form class="form-inline my-2 my-lg-0" method="GET">
                        <select class="form-control-sm" name="strThn">
                            <option value="-">-- Pilih Tahun --</option>
                            <?php
                            $qrythn = $mpi->query("select thn from tbl_thn")->fetchAll(PDO::FETCH_OBJ);
                            foreach ($qrythn as $a):
                                ?>
                                <option value="<?= $a->thn; ?>" <?= $strThn == $a->thn ? "selected" : ""; ?>><?= $a->thn; ?></option>
                                <?php
                            endforeach;
                            ?>
                        </select>
                        &nbsp;&nbsp;
                        <button class="btn btn-success my-2 my-sm-0" type="submit">Search</button>
                    </form>
                </div>
            </nav>
        </div>
        
        <div class="modal fade bd-example-modal-xl" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-xl">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="myExtraLargeModalLabel">New message</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body" id="mobo"></div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" id="smp">Simpan</button>
                    </div>
                </div>
            </div>
        </div>
        
        <br>
        <br>
        <br>
        <div class="container-fluid">
            <div class="card">
                <div class="card-header"><h5>Daftar Jadwal Pengerjaan.</h5></div>
                <div class="card-body">
                    <table class="table table-bordered table-hover table-sm table-primary table-striped">
                        <thead>
                            <tr>
                                <th width="5%">No.</th>
                                <th width="25%">Form</th>
                                <th width="60%">Keterangan</th>
                                <th width="10%">Opsi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $nom = 1;
                            $qryb = $mpi->query("select * from tbl_jdw where substring(nojdw, 4, 4)='{$strThn}' order by nojdw ASC")->fetchAll(PDO::FETCH_OBJ);
                            foreach ($qryb as $b):
                            $expjdw = explode("/", $b->nojdw);
                            
                            $week_array = week_range($expjdw[0], $expjdw[1]);
                            ?>
                            <tr>
                                <td><?= $nom; ?></td>
                                <td><?= $b->nojdw; ?> (<?= $week_array['week_start']." ~ ".$week_array['week_end']; ?>)</td>
                                <td>
                                    <?= $b->ket; ?>
                                    &nbsp;<a class="btn btn-default btn-sm" data-toggle="modal" onclick="pop(this.id);" id="<?= $expjdw[0]; ?>" data-target=".bd-example-modal-xl"><i class="fa fa-plus"></i></a>
                                    &nbsp;<a href="" class="btn btn-default btn-sm" data-toggle="collapse" id="<?= $expjdw[0]; ?>" onclick="clp(this.id);" data-target="#data<?= $expjdw[0]; ?>" aria-expanded="false" aria-controls="data<?= $expjdw[0]; ?>"><i class="fa fa-expand"></i></a>
                                    <div class="collapse" id="data<?= $expjdw[0]; ?>">
                                        <div class="card card-body" id="nospk<?= $expjdw[0]; ?>"></div>
                                    </div>
                                </td>
                                <td>
                                    <?php
                                    if($bagian == 1):
                                    ?>
                                        <a href="../../detail/dir/dtl_appjdw.php?nojdw=<?= $b->nojdw; ?>&nav=A" class="btn btn-info btn-sm"><i class="fa fa-search fa-sm"></i></a>
                                    <?php
                                    elseif($bagian == 13):
                                    ?>
                                        <a href="../../detail/dir/dtl_appjdw.php?nojdw=<?= $b->nojdw; ?>&nav=A" class="btn btn-info btn-sm"><i class="fa fa-search fa-sm"></i></a>
                                        <a href="../../view/ppic/view_spk.php?nojdw=<?= $b->nojdw; ?>" class="btn btn-danger btn-sm"><i class="fa fa-sm fa-search"></i></a>
                                    <?php
                                    else:
                                    ?>
                                        <a href="../../view/ppic/view_spk.php?nojdw=<?= $b->nojdw; ?>" class="btn btn-danger btn-sm"><i class="fa fa-sm fa-search"></i></a>
                                    <?php
                                    endif;
                                    ?>
                                </td>
                            </tr>
                            <?php
                            $nom++;
                            endforeach;
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </body>
</html>