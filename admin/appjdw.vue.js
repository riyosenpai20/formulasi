$("document").ready(function(){
    $("#smp").click(function(){
        var prmFrm = $("input[name='prmFrm']").val();
        if(prmFrm === "frmSpk"){
            var nospk = $("input[name='nospk']").val(),
                awl = $("input[name='awl']").val(),
                akr = $("input[name='akr']").val(),
                ktr = $("#ktr").val(),
                nojdw = $("input[name='nojdw']").val(),
                trn = $("input[name='trn']").val();

            $.ajax({
                url: "http://" + location.host + "/project1_5/custompage/produksi/act_jip.php?exe=smp&obj=spk",
                data: {
                    nospk: nospk,
                    awl: awl,
                    akr: akr,
                    ktr: ktr,
                    nojdw: nojdw,
                    trn: trn
                },
                type: "POST",
                cache: false,
                success: function(){
                    location.reload();
                }
            });
        }else if(prmFrm === "btljaker"){
            $.ajax({
               url: "http://" + location.host + "/project1_5/custompage/produksi/act_jip.php?exe=set&obj=btljaker",
                data: {
                    nojdw: $("#nojdw").val()
                },
                type: 'POST',
                cache: false,
                success: function (data) {
                    location.reload();
                }
            });
        }
    });
});

function app(nojdw){
    $.ajax({
        url: "http://" + location.host + "/project1_5/custompage/produksi/act_jip.php?exe=app&obj=jdw",
        data: {nojdw: nojdw},
        type: 'POST',
        cache: false,
        success: function (data) {
            location.reload();
        }
    });
}

function clp(data){
    var mgg = data.split("/"),
        strThn = $("select[name='strThn']").val();
    $.ajax({
        url: "http://" + location.host + "/project1_5/custompage/produksi/act_jip.php?exe=dtl&obj=spk",
        data: {
            mgg: mgg[0],
            strThn: strThn
        },
        type: 'POST',
        cache: false,
        success: function (isi) {
            $("#nospk" + mgg[0]).html(isi).show();
        }
    });
}

function pop(aidi){
    var strThn = $("select[name='strThn']").val();
    $.ajax({
        url: "http://" + location.host + "/project1_5/custompage/produksi/act_jip.php?exe=form&obj=spk",
        data: {
            id: aidi,
            strThn: strThn
        },
        type: 'POST',
        cache: false,
        success: function(data){
            $("#mobo").html(data).show();
        }
    });
}

function clk(aidi){
    $.ajax({
        url: "http://" + location.host + "/project1_5/custompage/produksi/act_jip.php?exe=set&obj=btl",
        data: {
            id: aidi,
            nojdw: $("#nojdw").val()
        },
        type: 'POST',
        cache: false,
        success: function(data){
            var float = document.getElementById("float");
            if(data === "1"){
                float.style.display = "block";
            }else{
                float.style.display = "none";
            }
        }
    });
}

function modal(){
    $.ajax({
        url: "http://" + location.host + "/project1_5/custompage/produksi/jip.php?exe=frm&obj=btljaker",
        data: {
            nojdw: $("#nojdw").val()
        },
        type: "POST",
        cache: false,
        success: function (data) {
            $("#mobo").html(data).show();
        }
    });
}

function cbtl(val){
    $.ajax({
        url: "http://" + location.host + "/project1_5/custompage/produksi/act_jip.php?exe=plh&obj=btl",
        data: {
            nojdw: $("#nojdw").val(),
            idb: val
        },
        type: 'POST',
        cache: false,
        success: function (data) {
            
        }
    });
}

function appbtl(nojdw){
    $.ajax({
        url: "http://" + location.host + "/project1_5/custompage/produksi/act_jip.php?exe=app&obj=btl",
        data: {
            nojdw: nojdw
        },
        type: 'POST',
        cache: false,
        success: function (data) {
            location.reload();
//            alert(data);
        }
    });
}