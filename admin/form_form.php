<?php
ini_set('max_execution_time', 0);
error_reporting(1);
date_default_timezone_set("Asia/Jakarta");
session_start();
require '../configdb.php';
$mpi = cfg_pdo::connect();

$act = $_GET['act'];
$now = date("Y-m-d");
$nof = $_GET['nof'];
$jamNow = date("H:i:s");
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Form Formulasi</title>
        <link href="../addon/bootstrap.min.css" type="text/css" rel="stylesheet"/>
        <link href="../addon/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet"/>
        <link href="../addon/jquery/jquery-ui-1.12.1/jquery-ui.min.css" rel="stylesheet" type="text/css"/>
        <script src="../addon/jquery/jquery-3.2.0.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="../addon/jquery/jquery-ui-1.12.1/jquery-ui.js"></script>
        <script type="text/javascript" src="../addon/bootstrap.min.js"></script>
        <script type="text/javascript" src="../addon/angular/angular.min.1.6.3.js"></script>
        <script type="text/javascript" src="../addon/bootstrap-notify-master/bootstrap-notify.min.js"></script>
        <script type="text/javascript" src="formulasi.js"></script>
        <script type="text/javascript">
            $(function (){
                <?php
                $qryb = $mpi->query("select `KodeItem`, nama, warna, type, `SaldoAkir` FROM tbl_daftar_item WHERE `stsDel`='F' && `typeItem`='11' ORDER BY nama ASC");
                $arrb = $qryb->fetchAll(PDO::FETCH_OBJ);
                ?>
                <?php if($act == "new" || $act == "edit" || $act == "dup" || $act == "desc" || $act == "addon" || $act == "addedit"): ?>
                var satad = <?php
                echo "[";
                    foreach($arrb as $bla):
                        $ki = $bla->KodeItem;
                        $qryc = $mpi->query("select haru FROM tbl_daftar_item WHERE `KodeItem`='$ki'");
                        $arrc = $qryc->fetch(PDO::FETCH_OBJ);
                        $harsat = $arrc->haru;

                        echo "{";
                            echo "jng: '".trim($bla->nama)."',";
                            echo "wrn: '".$bla->warna."',";
                            echo "tpe: '".$bla->type."',";
                            echo "sal: '".$bla->SaldoAkir."',";
                            echo "kde: '".$bla->KodeItem."',";
                            echo "hrg: '".$harsat."'";
                        echo "},";
                    endforeach;
                        echo "{";
                            echo "jng: '',";
                            echo "wrn: '',";
                            echo "tpe: '',";
                            echo "sal: '',";
                            echo "kde: '',";
                            echo "hrg: ''";

                        echo "}";
                        echo "]";
                ?>;
                $.each(satad, function (i, line){
                   line.value = line.jng;
               });
                $('#baku').autocomplete({
                    minLength: 1,
                    source: satad,
                    focus: function (event, ui){
                        $('#baku').val(ui.item.jng);
                        return false;
                    },
                    select: function (event, ui){
                        $('#baku').val(ui.item.kde);
                        return  false;
                    }
                }).data("ui-autocomplete")._renderItem = function (ul, item){
                    return $("<li>").append("<a>" + item.jng + " " + item.wrn + " " + "("+ item.sal +") "+ "("+ item.hrg +")"+ "</a>")
                            .appendTo(ul);
                        };
                        <?php
                        endif;
                        ?>
            });
            <?php
            if($act == "edit" || $act == "addedit"):
            ?>
                    $(document).ready(function (){
                        $('#form').load("http://<?php echo $_SERVER['SERVER_NAME']; ?>/project1_5/custompage/dir/formulasi.php?nof=<?php echo $nof; ?>&exe=load&obj=dtl");
                    });
            <?php
            endif;
            ?>
        </script>
    </head>
    <body>
        <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" id="pop">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <div class="close" type="button" data-dismiss="modal"><span>&times;</span></div>
                        <h3 id="kepala"></h3>
                    </div>
                    <div class="modal-body"></div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" id="dlk">Simpan</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        <?php
        if($act == "new"):
            $qrya = $mpi->query("select formulasi FROM tbl_prm");
            $arra = $qrya->fetch(PDO::FETCH_OBJ);
            $no = explode("/",$arra->formulasi);

            if($no[1] == date("m")):
                $kof = $no[0]+1;
            else:
                $kof = 1;
            endif;
            ?>
            </br>
            <div class="panel panel-info">
                <div class="panel-heading"></div>
                <div class="panel-body">
                    <input type="hidden" id="nof" value="<?php echo str_pad($kof, 4, "0", STR_PAD_LEFT); ?>/<?php echo date("m"); ?>/<?php echo date("Y"); ?>" name="nof"/>
                    <div class="col-md-12">
                        <div class="col-md-12">
                            <div class="form-group col-md-3">
                                <label class="label-control col-md-4">Nama</label>
                                <div class="input-group">
                                    <input type="text" id="nm" class="form-control" style="text-transform:uppercase" name="nm" ng-model="prj.nm"/>
                                    <span class="input-group-addon">
                                        <span></span>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group col-md-3">
                                <label class="label-control col-md-4">Cobaan</label>
                                <div class="input-group">
                                    <input name="cb" class="form-control" style="text-transform:uppercase" ng-model="prj.cb" id="cb"/>
                                    <span class="input-group-addon">
                                        <span></span>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group col-md-3">
                                <label class="label-control col-md-4">Warna</label>
                                <div class="input-group">
                                    <select name="wr" class="form-control" id="wr">
                                        <option value="WARNA">WARNA</option>
                                        <option value="HITAM">HITAM</option>
                                    </select>
                                    <span class="input-group-addon">
                                        <span></span>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group col-md-3">
                                <label class="label-control col-md-4">Tanggal</label>
                                <div class="input-group">
                                    <input name="tg" class="form-control" ng-model="prj.wr" id="tg" value="<?php echo $now; ?>"/>
                                    <span class="input-group-addon">
                                        <span><i class="fa fa-calendar"></i></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-footer text-center">
                    <button class="btn btn-default" type="button" id="nxt"><i class="fa fa-angle-double-right"></i></button>
                </div>
            </div>
            <div class="col-md-12">
                <div class="input-group">
                    <input name="baku" class="form-control" id="baku"/>
                    <span class="input-group-addon" id="ref">
                        <span><i class="fa fa-refresh"></i></span>
                    </span>
                </div>
            </div>
            <div class="col-md-12" id="form"></div>
            <?php
        elseif($act == "edit"):
            $qryf = $mpi->query("select * FROM tbl_formulasi WHERE `KodeForm`='$nof'");
            $arrf = $qryf->fetch(PDO::FETCH_OBJ);
            ?>
            </br>
            <input id="nof" type="hidden" value="<?php echo $nof; ?>"/>
            <div class="panel panel-info">
                <div class="panel-heading"></div>
                <div class="panel-body">
                    <div class="col-md-12">
                        <div class="col-md-12">
                            <div class="form-group col-md-3">
                                <label class="label-control col-md-4">Nama</label>
                                <div class="input-group"><?php echo $arrf->nama; ?></div>
                            </div>
                            <div class="form-group col-md-3">
                                <label class="label-control col-md-4">Cobaan</label>
                                <div class="input-group"><?php echo $arrf->NoUrt; ?></div>
                            </div>
                            <div class="form-group col-md-3">
                                <label class="label-control col-md-4">Warna</label>
                                <div class="input-group"><?php echo $arrf->warna; ?></div>
                            </div>
                            <div class="form-group col-md-3">
                                <label class="label-control col-md-4">Tanggal</label>
                                <div class="input-group"><?php echo $arrf->tanggal; ?></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="input-group">
                    <input name="baku" class="form-control" id="baku"/>
                    <span class="input-group-addon" id="ref">
                        <span><i class="fa fa-refresh"></i></span>
                    </span>
                </div>
            </div>
            <div class="col-md-12" id="form"></div>
            <?php
        elseif($act == "dup"):
            $qrya = $mpi->query("select formulasi FROM tbl_prm");
            $arra = $qrya->fetch(PDO::FETCH_OBJ);

            $no = explode("/", $arra->formulasi);
            if($no[1] == date("m")):
                $kof = $no[0]+1;
            else:
                $kof = 1;
            endif;
            $NoForm = str_pad($kof, 4, "0", STR_PAD_LEFT)."/".date("m")."/".date("Y");

            $qry1 = $mpi->prepare("update tbl_prm SET formulasi=?");
            $qry1->execute(array($NoForm));

            $qryb = $mpi->query("select bahan, phr, NoUrt, harsat, kantong, pers FROM tbl_formulasidtl WHERE `KodeForm`='$nof'");
            $arrb = $qryb->fetchAll(PDO::FETCH_OBJ);

            $qryc = $mpi->prepare("insert INTO tbl_formulasi (`KodeForm`, tanggal) VALUES (?, ?)");
            $qryc->execute(array($NoForm, $now." ".$jamNow));

            foreach($arrb as $a):
                $qry2 = $mpi->prepare("insert INTO tbl_formulasidtl (`KodeForm`, bahan, phr, `NoUrt`, tgl, harsat, kantong, pers) VALUES (?, ?, ?, ?, ?, ?, ?, ?)");
                $qry2->execute(array($NoForm, $a->bahan, $a->phr, $a->NoUrt, $now." ".$jamNow, $a->harsat, $a->kantong, $a->pers));
            endforeach;
            ?>
            <script type="text/javascript">
                $(document).ready(function (){
                    $('#form').load("http://<?php echo $_SERVER['SERVER_NAME']; ?>/formulasi/admin/formulasi.php?nof=<?php echo $NoForm; ?>&exe=load&obj=dtl");
                });
            </script>
            <div class="panel panel-info">
                <div class="panel-heading"></div>
                <div class="panel-body">
                    <input type="hidden" id="nof" value="<?php echo $NoForm; ?>"/>
                    <div class="col-md-12">
                        <div class="form-group col-md-3">
                            <label class="label-control col-md-4">Nama</label>
                            <div class="input-group">
                                <input type="text" id="nm" class="form-control" style="text-transform:uppercase" name="nm"/>
                                <span class="input-group-addon">
                                    <span></span>
                                </span>
                            </div>
                        </div>
                        <div class="form-group col-md-3">
                            <label class="label-control col-md-4">Cobaan</label>
                            <div class="input-group">
                                <input name="cb" class="form-control" style="text-transform:uppercase" ng-model="prj.cb" id="cb"/>
                                <span class="input-group-addon">
                                    <span></span>
                                </span>
                            </div>
                        </div>
                        <div class="form-group col-md-3">
                            <label class="label-control col-md-4">Warna</label>
                            <div class="input-group">
                                <select name="wr" class="form-control" id="wr">
                                    <option value="WARNA">WARNA</option>
                                    <option value="HITAM">HITAM</option>
                                </select>
                                <span class="input-group-addon">
                                    <span></span>
                                </span>
                            </div>
                        </div>
                        <div class="form-group col-md-3">
                            <label class="label-control col-md-4">Tanggal</label>
                            <div class="input-group">
                                <input name="tg" class="form-control" ng-model="prj.wr" id="tg" value="<?php echo $now; ?>"/>
                                <span class="input-group-addon">
                                    <span><i class="fa fa-calendar"></i></span>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-footer text-center">
                    <button class="btn btn-default" type="button" id="svh"><i class="fa fa-angle-double-right"></i></button>
                </div>
                <div class="col-md-12">
                    <div class="input-group">
                        <input name="baku" class="form-control" id="baku"/>
                        <span class="input-group-addon" id="ref">
                            <span><i class="fa fa-refresh"></i></span>
                        </span>
                    </div>
                </div>
            </div>
            <div class="col-md-12" id="form"></div>
            <?php
        elseif($act == "desc"):
            $qryg = $mpi->query("select karet, exp, warna FROM tbl_formulasi WHERE `KodeForm`='$nof'");
            $arrg = $qryg->fetch(PDO::FETCH_OBJ);

            $qryh = $mpi->query("select formulasi FROM tbl_prm");
            $arrh = $qryh->fetch(PDO::FETCH_OBJ);
            $no = explode("/", $arrh->formulasi);
            if($no[1] == date("m")):
                $kof = $no[0]+1;
            else:
                $kof = 1;
            endif;

            $qryi = $mpi->query("select NoUrt FROM tbl_formulasi WHERE karet='".$arrg->karet."' && exp='".$arrg->exp."' && warna='".$arrg->warna."' && `stsDel`='F' ORDER BY id DESC limit 0,1");
            $arri = $qryi->fetch(PDO::FETCH_OBJ);
            $nou = $arri->NoUrt;
            if(empty($nou)):
                $NoUrut = 1;
            else:
                $NoUrut = $nou+1;
            endif;

            $qry1 = $mpi->prepare("insert INTO tbl_formulasi (`KodeForm`, karet, exp, warna, uline, tanggal, nama, `NoUrt`) VALUES (?, ?, ?, ?, ?, ?, ?, ?)");
            $qry1->execute(array(str_pad($kof, 4, "0", STR_PAD_LEFT)."/".  date("m")."/".  date("Y"), $arrg->karet, $arrg->exp, $arrg->warna, $nof, $now." ".$jamNow, $arrg->karet." ".$arrg->exp, $NoUrut));

            $NoForm = str_pad($kof, 4, "0", STR_PAD_LEFT)."/".  date("m")."/".  date("Y");

            $qry2 = $mpi->prepare("update tbl_prm SET formulasi=?");
            $qry2->execute(array($NoForm));

            $qryj = $mpi->query("select nama, `NoUrt`, warna, tanggal FROM tbl_formulasi WHERE `KodeForm`='$NoForm'");
            $arrj = $qryj->fetch(PDO::FETCH_OBJ);

            $qryk = $mpi->query("select bahan, phr, `NoUrt`, harsat, kantong, pers FROM tbl_formulasidtl WHERE `KodeForm`='$nof'");
            $arrk = $qryk->fetchAll(PDO::FETCH_OBJ);

            foreach ($arrk as $k):
                $qry3 = $mpi->prepare("insert INTO tbl_formulasidtl (`KodeForm`, bahan, phr, `NoUrt`, tgl, harsat, kantong, pers) VALUES (?, ?, ?, ?, ?, ?, ?, ?)");
                $qry3->execute(array($NoForm, $k->bahan, $k->phr, $k->NoUrt, $now." ".$jamNow, $k->harsat, $k->kantong, $k->pers));
            endforeach;
            ?>
            <script type="text/javascript">
                $(document).ready(function (){
                    $('#form').load("http://<?php echo $_SERVER['SERVER_NAME']; ?>/formulasi/admin/formulasi.php?nof=<?php echo $NoForm; ?>&exe=load&obj=dtl");
                });
            </script>
            <input type="hidden" id="nof" value="<?php echo $NoForm; ?>"/>
            <div class="col-md-12">
                <div class="form-group col-md-4">
                    <label class="label-control col-md-4">Nama</label>
                    <div class="input-group"><?php echo $arrj->nama."-".$arrj->NoUrt; ?></div>
                </div>
                <div class="form-group col-md-4">
                    <label class="label-control col-md-4">Warna</label>
                    <div class="input-group"><?php echo $arrj->warna; ?></div>
                </div>
                <div>
                    <label class="label-control col-md-4">tanggal</label>
                    <div class="input-group"><?php echo $arrj->tanggal; ?></div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="input-group">
                    <input name="baku" class="form-control" id="baku"/>
                    <span class="input-group-addon" id="ref">
                        <span><i class="fa fa-refresh"></i></span>
                    </span>
                </div>
            </div>
            <div class="col-md-12" id="form"></div>
            <?php
        elseif($act == "view"):
            $qryd = $mpi->query("select * FROM tbl_formulasi WHERE `KodeForm`='$nof'");
            $arrd = $qryd->fetch(PDO::FETCH_OBJ);

            $stsTrl = $arrd->tglTrial;
            $stsUpg = $arrd->tglUpg;
            $stsVal = $arrd->val;
            $ul = $arrd->uline;
            $qtyTrl = $arrd->qtyTrl;
            if(!empty($ul)):
                $qryf = $mpi->query("select nama, `KodeForm`, warna, `NoUrt` FROM tbl_formulasi WHERE `KodeForm`='$ul'");
                $arrf = $qryf->fetch(PDO::FETCH_OBJ);
                $namf = $arrf->nama;
                $noff = $arrf->KodeForm;
                $norf = $arrf->NoUrt;
            endif;
            ?>
            <input name="nof" type="hidden" id="nof" value="<?php echo $nof; ?>"/>
            <div class="col-md-12">
                <?php
                if(!empty($ul)):
                ?>
                <div class="form-group col-md-3">
                    <label class="label-control col-md-4">Lama</label>
                    <div class="input-group"><a class="btn btn-info btn-xs" href="form_form.php?act=view&nof=<?php echo $noff; ?>"><?php echo $namf."-".$norf; ?></a></div>
                </div>
                <?php
                endif;
                ?>
                <div class="form-group col-md-3">
                    <label class="label-control col-md-4">Nama</label>
                    <div class="input-group"><?php echo $arrd->nama."-".$arrd->NoUrt; ?></div>
                </div>
                <div class="form-group col-lg-3">
                    <label class="label-control col-md-4">Warna</label>
                    <div class="input-group"><?php echo $arrd->warna; ?></div>
                </div>
                <div class="form-group col-lg-3">
                    <label class="label-control col-md-4">Tanggal</label>
                    <div class="input-group"><?php echo $arrd->tanggal; ?></div>
                </div>
                <input type="hidden" id="kar" value="<?php echo $arrd->karet; ?>"/>
            </div>
            <br>
            <?php
            $qrye = $mpi->query("select a.*, b.nama FROM tbl_formulasidtl as a left join tbl_daftar_item as b on a.bahan=b.`KodeItem` WHERE `KodeForm`='$nof'");
            $arre = $qrye->fetchAll(PDO::FETCH_OBJ);
            ?>
            <div class="col-md-12">
                <div class="panel panel-info">
                    <div class="panel-heading"></div>
                    <div class="panel-body">
                        <table class="table table-bordered table-hover table-condensed table-striped">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Nama</th>
                                    <th>Phr</th>
                                    <th>Kantong</th>
                                    <th>@Harga (kg)</th>
                                    <th>Harga Total</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $no = 1;
                                foreach ($arre as $b):
                                    $ki = $b->bahan;
                                    $ktg = $b->kantong;
                                    $qryc = $mpi->query("select haru FROM tbl_daftar_item WHERE `KodeItem`='$ki'");
                                    $arrc = $qryc->fetch(PDO::FETCH_OBJ);
                                    $harsat = $arrc->haru;
                                    $hartot = $b->phr * $harsat;
                                    $x[] = $b->phr;
                                    $y[] = $hartot;
                                    ?>
                                    <tr>
                                        <td><?php echo $no; ?></td>
                                        <td><?php echo $b->nama; ?></td>
                                        <td><?php echo $b->phr; ?></td>
                                        <td><?php echo $ktg; ?></td>
                                        <td><?php echo number($harsat,2,",","."); ?></td>
                                        <td><?php echo number($hartot, 2, ",", "."); ?></td>
                                    </tr>
                                    <?php
                                    $no++;
                                endforeach;
                                ?>
                                    <tr>
                                        <td colspan="5" class="text-right"><strong>Jumlah Harga Total</strong></td>
                                        <td><?php echo round(array_sum($y),2); ?></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" class="text-right"><strong>Jumlah PHR</strong></td>
                                        <td><?php echo array_sum($x); ?></td>
                                        <td></td>
                                        <td class="text-right"><strong>Biaya per phr</strong></td>
                                        <td><?php echo round((array_sum($y)/  array_sum($x)),2); ?></td>
                                    </tr>
                            </tbody>
                        </table>
                        <?php
                        if(empty($stsTrl) && empty($stsUpg)):
                        ?>
                        <a class="btn btn-primary" id="trial" title="Trial" href="#pop" data-toggle="modal"><i class="fa fa-text-width"></i></a>
                        <?php
                        elseif(empty($stsTrl) && !empty($stsUpg)):

                        endif;
                        if(empty($stsTrl) && empty($stsUpg)):
                        ?>
                        <a class="btn btn-success" id="upg" title="Upgrade" href="#pop" data-toggle="modal"><i class="fa fa-level-up"></i></a>
                        <?php
                        elseif(!empty($stsTrl) && empty($stsUpg)):
                           ?>
                        <a class="btn btn-success" id="upg" title="upgrade" href="#pop" data-toggle="modal"><i class="fa fa-level-up"></i></a>
                            <?php
                        elseif(empty($stsTrl) && !empty($stsUpg)):

                        endif;
                        if(!empty($stsTrl) && empty($stsVal)):
                        ?>
                        <button type="button" class="btn btn-success" title="OK" id="btnOK"><i class="fa fa-check-square-o"></i></button>
                        <?php
                        elseif(!empty($stsTrl) && $stsVal ==1):

                        endif;
                        if(!empty($stsTrl) && empty($stsVal)):
                        ?>
                        <button type="button" class="btn btn-danger" title="NOT OK" id="btnNO"><i class="fa fa-window-close-o"></i></button>
                        <?php
                        elseif(!empty($stsTrl) && $stsVal ==2):

                        endif;
                        ?>
                    </div>
                </div>
            </div>
            <?php
            if(!empty($stsTrl) || empty($stsTrl) && !empty($stsUpg)):
            ?>
            <div class="col-md-12">
                <div class="panel panel-primary">
                    <div class="panel-heading"></div>
                    <div class="panel-body">
                        <table class="table table-condensed">
                            <tr>
                                <td>Nama :</td>
                                <td><?php echo $arrd->nama."-".$arrd->NoUrt; ?></td>
                            </tr>
                            <tr>
                                <td>Tambahan :</td>
                                <td>
                                    <?php
                                    $qrym = $mpi->query("select b.nama FROM tbl_tryinc as a left join tbl_addon as b on a.`KodeAddon`=b.`KodeAddon` WHERE a.`KodeForm`='$nof'");
                                    $arrm = $qrym->fetchAll(PDO::FETCH_OBJ);
                                    foreach ($arrm as $m):
                                        echo $m->nama."<br>";
                                    endforeach;
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <td>Qty Trial :</td>
                                <td>
                                    <?php
                                    echo $qtyTrl." kg";
                                    ?>
                                </td>
                            </tr>
                            <?php
                            if(isset($arrd->NmJd)):
                            ?>
                            <tr>
                                <td>Nama Jadi :</td>
                                <td><?php echo $arrd->NmJd; ?></td>
                            </tr>
                            <?php
                            endif;
                            ?>
                            <tr>
                                <td>Warna :</td>
                                <td><?php echo $arrd->warna; ?></td>
                            </tr>
                            <tr>
                                <td>HS Tester :</td>
                                <td>
                                    <?php
                                        echo $arrd->hst1."-".$arrd->hst2."-".$arrd->hst3;
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <td>HS Roll :</td>
                                <td>
                                    <?php
                                        echo $arrd->hsr1."-".$arrd->hsr2."-".$arrd->hsr3;
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <td>Tensile :</td>
                                <td><?php echo $arrd->Tensile; ?></td>
                            </tr>
                            <tr>
                                <td>Abrassion :</td>
                                <td><?php echo $arrd->abrasion; ?></td>
                            </tr>
                            <tr>
                                <td>Hasil :</td>
                                <td><?php echo $arrd->hasil; ?></td>
                            </tr>
                            <?php
                            if(!empty($arrd->keterangan)):
                            ?>
                            <tr>
                                <td>Catatan :</td>
                                <td><?php echo $arrd->keterangan; ?></td>
                            </tr>
                            <?php
                            else:
                                ?>
                            <tr>
                                <td>Catatan :</td>
                                <td><input type="text" name="cat" value="<?php ?>" placeholder="hasil ketik otomatis tersimpan" id="cat" onkeyup="catt(this.value)" class="form-control"/></td>
                            </tr>
                            <?php
                            endif;
                            ?>
                        </table>
                    </div>
                </div>
            </div>
            <?php
            endif;
        elseif($act == "rnd"):
            $qryc = $mpi->query("select nama, NoUrt, `NmJd`, warna, hst1, hst2, hst3, hsr1, hsr2, hsr3, hasil, keterangan, `Tensile`, abrasion, catatan, `tglRnD` FROM tbl_formulasi WHERE `KodeForm`='$nof'");
            $arrc = $qryc->fetch(PDO::FETCH_OBJ);
            ?>
            <form action="formulasi.php?exe=sv&obj=hsl" method="POST">
                <div class="col-md-8 col-md-offset-2">
                    <div class="panel panel-primary">
                        <div class="panel-heading">Input data :</div>
                        <div class="panel-body">
                            <input type="hidden" name="nof" value="<?php echo $nof; ?>"/>
                            <table class="table table-condensed">
                                <tr>
                                    <td>Nama :</td>
                                    <td><?php echo $arrc->nama."-".$arrc->NoUrt; ?></td>
                                </tr>
                                <tr>
                                    <td>Tambahan :</td>
                                    <td>
                                        <?php
                                        $qryl = $mpi->query("select b.nama FROM tbl_tryinc as a left join tbl_addon as b on a.`KodeAddon`=b.`KodeAddon` WHERE a.`KodeForm`='$nof'");
                                        $arrl = $qryl->fetchAll(PDO::FETCH_OBJ);
                                        foreach ($arrl as $l):
                                            echo $l->nama."<br>";
                                        endforeach;
                                        ?>
                                    </td>
                                </tr>
                                <?php
                                if(isset($arrc->NmJd)):
                                ?>
                                <tr>
                                    <td>Nama Jadi :</td>
                                    <td><?php echo $arrc->NmJd; ?></td>
                                </tr>
                                <?php
                                endif;
                                ?>
                                <tr>
                                    <td>Warna :</td>
                                    <td><?php echo $arrc->warna; ?></td>
                                </tr>
                                <tr>
                                    <td>HS Tester :</td>
                                    <td>
                                        <?php
                                        if(!empty($arrc->hst1)):
                                            echo $arrc->hst1."-".$arrc->hst2."-".$arrc->hst3;
                                        else:
                                            ?>
                                            <div class="col-md-12">
                                                <div class="col-md-4">
                                                    <input name="hst1" class="form-control"/>
                                                </div>
                                                <div class="col-md-4">
                                                    <input name="hst2" class="form-control"/>
                                                </div>
                                                <div class="col-md-4">
                                                    <input name="hst3" class="form-control"/>
                                                </div>
                                            </div>
                                            <?php
                                        endif;
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>HS Roll :</td>
                                    <td>
                                        <?php
                                        if(!empty($arrc->hsr1)):
                                            echo $arrc->hsr1."-".$arrc->hsr2."-".$arrc->hsr3;
                                        else:
                                            ?>
                                            <div class="col-md-12">
                                                <div class="col-md-4">
                                                    <input name="hsr1" class="form-control"/>
                                                </div>
                                                <div class="col-md-4">
                                                    <input name="hsr2" class="form-control"/>
                                                </div>
                                                <div class="col-md-4">
                                                    <input name="hsr3" class="form-control"/>
                                                </div>
                                            </div>
                                            <?php
                                        endif;
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Tensile :</td>
                                    <td><?php echo empty($arrc->Tensile)?"<input name='tensile' class='form-control'/>":$arrc->Tensile; ?></td>
                                </tr>
                                <tr>
                                    <td>Abrassion :</td>
                                    <td><?php echo empty($arrc->abrasion)?"<input name='abbr' class='form-control'/>":$arrc->abrasion; ?></td>
                                </tr>
                                <tr>
                                    <td>Hasil :</td>
                                    <td><?php echo empty($arrc->hasil)?"<input name='hsl' class='form-control'/>":$arrc->hasil; ?></td>
                                </tr>
                                <?php
                                if(isset($arrc->catatan)):
                                ?>
                                <tr>
                                    <td>Catatan :</td>
                                    <td><?php echo $arrc->catatan; ?></td>
                                </tr>
                                <?php
                                endif;
                                ?>
                            </table>
                            <?php
                            if(empty($arrc->tglRnD)):
                            ?>
                            <button type="submit" class="btn btn-primary"><i class="fa fa-arrow-right"></i></button>
                            <?php
                            endif;
                            ?>
                        </div>
                    </div>
                </div>
            </form>
            <?php
        elseif($act == "addon"):
            $qrya = $mpi->query("select addon FROM tbl_prm");
            $arra = $qrya->fetch(PDO::FETCH_OBJ);
            $no = explode("/",$arra->addon);

            if($no[1] == date("m")):
                $kof = $no[0]+1;
            else:
                $kof = 1;
            endif;
            ?>
            </br>
            <div class="panel panel-info">
                <div class="panel-heading"></div>
                <div class="panel-body">
                    <input type="hidden" id="nof" value="<?php echo str_pad($kof, 4, "0", STR_PAD_LEFT); ?>/<?php echo date("m"); ?>/<?php echo date("Y"); ?>/<?php echo "A"; ?>" name="nof"/>
                    <div class="col-md-12">
                        <div class="col-md-12">
                            <div class="form-group col-md-3">
                                <label class="label-control col-md-4">Nama</label>
                                <div class="input-group">
                                    <input type="text" id="nm" style="text-transform:uppercase" class="form-control" name="nm" ng-model="prj.nm"/>
                                    <span class="input-group-addon">
                                        <span></span>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group col-md-3">
                                <label class="label-control col-md-4">Type</label>
                                <div class="input-group">
                                    <select name="tp" class="form-control" id="tp">
                                        <option value="WARNA">WARNA</option>
                                        <option value="ADDIN">ADD-IN</option>
                                    </select>
                                    <span class="input-group-addon">
                                        <span></span>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group col-md-3">
                                <label class="label-control col-md-4">Tanggal</label>
                                <div class="input-group">
                                    <input name="tg" class="form-control" ng-model="prj.wr" id="tg" value="<?php echo $now; ?>"/>
                                    <span class="input-group-addon">
                                        <span><i class="fa fa-calendar"></i></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-footer text-center">
                    <button class="btn btn-default" type="button" id="nxton"><i class="fa fa-angle-double-right"></i></button>
                </div>
            </div>
            <div class="col-md-12">
                <div class="input-group">
                    <input name="bakuon" class="form-control" id="baku"/>
                    <span class="input-group-addon" id="refadd">
                        <span><i class="fa fa-refresh"></i></span>
                    </span>
                </div>
            </div>
            <div class="col-md-12" id="form"></div>
            <?php
        elseif($act == "chg"):
            ?>
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-primary">
                    <div class="panel-heading"></div>
                    <div class="panel-body">
                        <div class="form-group col-md-6">
                            <label class="label-control col-md-4">Bahan Lama :</label>
                            <div class="input-group">
                                <select class="form-control" name="awal" id="awal">
                                    <?php
                                    $qryc = $mpi->query("select nama, `KodeItem` FROM tbl_daftar_item WHERE `typeItem`='11' && `stsDel`='F'");
                                    $arrc = $qryc->fetchAll(PDO::FETCH_OBJ);
                                    foreach ($arrc as $c):
                                        $ki = $c->KodeItem;
                                        $nmk = $c->nama;
                                        echo "<option value='$ki'>$nmk</option>";
                                    endforeach;
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="label-control col-md-4">Bahan Baru :</label>
                            <div class="input-group">
                                <select class="form-control" name="baru" id="baru">
                                    <?php
                                    $qryd = $mpi->query("select nama, `KodeItem` FROM tbl_daftar_item WHERE `typeItem`='11' && `stsDel`='F'");
                                    $arrd = $qryd->fetchAll(PDO::FETCH_OBJ);
                                    foreach ($arrd as $d):
                                        $ki1 = $d->KodeItem;
                                        $nmk1 = $d->nama;
                                        echo "<option value='$ki1'>$nmk1</option>";
                                    endforeach;
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div id="data"></div>
            </div>
            <div class="col-md-12">
                <button type="button" class="btn btn-primary" id="gnt"><i class="fa fa-save"></i></button>
            </div>
            <?php
        elseif($act == "addedit"):
            $qrya = $mpi->query("select * FROM tbl_addon WHERE `KodeAddon`='$nof'");
            $arra = $qrya->fetch(PDO::FETCH_OBJ);
            $nm = $arra->nama;
            $tp = $arra->type;
            $tg = $arra->tgl;
            ?>
            <br>
            <div class="panel panel-info">
                <div class="panel-heading"></div>
                <div class="panel-body">
                    <input type="hidden" id="nof" value="<?php echo $nof; ?>" name="nof"/>
                    <div class="col-md-12">
                        <div class="form-group col-md-3">
                            <label class="label-control col-md-4">Nama</label>
                            <div class="input-group"><?php echo $nm; ?></div>
                        </div>
                        <div class="form-group col-md-3">
                            <label class="label-control col-md-4">Type</label>
                            <div class="input-group"><?php echo $tp; ?></div>
                        </div>
                        <div class="form-group col-md-3">
                            <label class="label-control col-md-4">Tanggal</label>
                            <div class="input-group"><?php echo $tg; ?></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="input-group">
                    <input name="bakuon" class="form-control" id="baku"/>
                    <span class="input-group-addon" id="refadd">
                        <span><i class="fa fa-refresh"></i></span>
                    </span>
                </div>
            </div>
            <div class="col-md-12" id="form"></div>
            <?php
        endif;
        ?>
    </body>
</html>
