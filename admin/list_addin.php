<?php
error_reporting(0);
include '../../fungsi.php';
include '../../configdb.php';
$mpi = cfg_pdo::connect();
$mli = cfg_sqli();
open_connect();

$username = $_SESSION['username2'];
$strNIK = $_SESSION['NIK'];
$strBulan = "strBulan";
$strTahun = "strTahun";

//set page size
$PageSize = 10;
$StartRow = 0;

//bagian
$qry1 = $mpi->query("select bagian, jabatan FROM tbl_kry_departemen WHERE aktif='T' && `NIK`='$strNIK'");
$bgn = $qry1->fetch(PDO::FETCH_OBJ);

$bagian = $bgn->bagian;
$jabatan = $bgn->jabatan;

//set page no
if(empty($_GET['PageNo'])):
    if($StartRow == 0):
        $PageNo = $StartRow + 1;
    endif;
else:
    $PageNo = $_GET['PageNo'];
    $StartRow = ($PageNo - 1) * $PageSize;
endif;

//set the counter start
if($PageNo % $PageSize == 0):
    $CounterStart = $PageNo - ($PageSize - 1);
else:
    $CounterStart = $PageNo - ($PageNo % $PageSize) + 1;
endif;

//Counter End
$CounterEnd = $CounterStart + ($PageSize - 1);
?>
<!DOCTYPE html>
<html>
    <head>
        <title>List Add-On Formulasi</title>
        <link type="text/css" href="../../bootstrap/css/bootstrap.min.css" rel="stylesheet"/>
        <link type="text/css" href="../../angular/angular-material.min.css" rel="stylesheet"/>
        <link type="text/css" rel="stylesheet" href="../../add-in/font-awesome-4.7.0/css/font-awesome.min.css"/>
        <script type="text/javascript" src="../../jquery/jquery-1.12.1.min.js"></script>
        <script type="text/javascript" src="../../bootstrap/js/bootstrap.min.js"></script>
    </head>
    <body>
        <?php
        if(isset($_GET['txtKeyword'])):
            $keyword = $_GET["txtKeyword"];
        else:
            $keyword = NULL;
        endif;
        
        //-------------------------------
        $tgl = date('Y-m-d');
        $thn = substr($tgl,0,4);
        $newdate = strtotime('-365 day',strtotime($tgl));
        $newdate = date('Y-m-d', $newdate);
        //--------------------------------
        if(isset($_GET['strBln'])):
            $strBln = $_GET['strBln'];
        else:
            $strBln = date('m');
        endif;
        //---------------------------------
        if(isset($_GET['strThn'])):
            $strThn = $_GET['strThn'];
        else:
            $strThn = date('Y');
        endif;
        //---------------------------------
        if(isset($_GET['type'])):
            $strType = $_GET['type'];
        else:
            $strType = NULL;
        endif;
        //---------------------------------
        if(isset($_GET['chkthn'])):
            $chkthn = $_GET['chkthn'];
        else:
            $chkthn = NULL;
        endif;
        //---------------------------------
        if($chkthn=="T"):
            $checked = "checked";
            $filterbln="";
        else:
            $checked = "";
            $filterbln="AND SUBSTR(a.tgl,6,2)='$strBln'";
        endif;
        //----------------------------------
        if(isset($_GET['strStatus'])):
            $strStatus = $_GET['strStatus'];
        else:
            $strStatus = NULL;
        endif;
        //-----------------------------------
        if(isset($_GET['chkthn']) && $_GET['chkthn']=="T"):
            $filterTgl = NULL;
            $checked="checked='checked'";
        else:
            $filterTgl = "&& substr(a.tgl,6,2) like '%".$strBln."%'";
            $checked=NULL;
        endif;
        
        //-------------------------------------
        $strSQL = "select * FROM tbl_addon WHERE `stsDel`='F'";
        $objQuery = $mpi->query($strSQL);
        $RecordCount = $objQuery->rowCount();
        $MaxPage = $RecordCount % $PageSize;
        //-------------------------------------
        if($RecordCount % $PageSize == 0):
            $MaxPage = $RecordCount / $PageSize;
        else:
            $MaxPage = ceil($RecordCount / $PageSize);
        endif;
        //------------------------------------
        $strSQL .="order  by id ASC LIMIT $StartRow , $PageSize";
        $objQuery  = $mpi->query($strSQL);
        ?>
        <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" id="pop">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <div class="close" type="button" data-dismiss="modal"><span>&times;</span></div>
                        <h3 id="kepala"></h3>
                    </div>
                    <div class="modal-body"></div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Tutup</button>
                    </div>
                </div>
            </div>
        </div>
        <div id="content" align="center">
            <div id="dhtmltooltip">
                <div align="left" id="content">
                    <nav class="navbar navbar-default navbar-fixed-top">
                        <div class="container-fluid">
                            <div class="navbar-form" role="search">
                                <div class="form-group">
                                    <form action="<?php echo $_SERVER['SCRIPT_NAME']; ?>" method="get">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="3">
                                            <tr>
                                                <td width="3%"> &nbsp;&nbsp; Cari : &nbsp;&nbsp;
                                                    <input class="form-control" type="text" name="txtKeyword" size="25" maxlength="25" value="<?php echo $keyword ?>"/>&nbsp;
                                                    <?php echo combo_db("strBln","SELECT twodigit, nm_bln FROM tbl_bln",$strBln);?>&nbsp;
                                                    <?php echo combo_db("strThn","SELECT thn, thn_id FROM tbl_thn",$strThn);?>&nbsp;                                                    
                                                    <input type="checkbox" name="chkthn" value="T" <?php echo $checked;?>/>&nbsp;
                                                    <span>Per Tahun</span>
                                                    &nbsp;&nbsp; 
                                                    &nbsp;
                                                    <button name="submit" type="submit" class="btn btn-success" id="Submit" value=""><i class="glyphicon glyphicon-search"></i></button>
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a type="button" class="btn btn-info" href="../../form/dir/form_form.php?act=addon&strThn=<?php echo $strThn; ?>&strBln=<?php echo $strBln; ?>"><i class="glyphicon glyphicon-plus"></i></a>
                                                </td>
                                            </tr>
                                        </table>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </nav>
                    <br/>
                    <div class="panel panel-info">
                        <div class="panel-heading" align="center"><b>List Add-On</b></div>
                        <div align="right" id="style_record"><strong><?php echo "Ada <span class='badge'>$RecordCount</span> record - Halaman <span class='badge'>$PageNo</span> dari <span class='badge'>$MaxPage</span>"; ?></strong></div>
                        <div class="panel-body">
                            <form name="FrmRjc" method="post" action="#">
                                <div class="panel panel-default">
                                    <table class="table table-bordered table-hover table-striped table-condensed" id="rjc" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th colspan="10"><center>List Add-On</center></th>
                                            </tr>
                                            <tr>
                                                <th width="2%"><center>No.</center></th>
                                                <th><center>Nama Add-On</center></th>
                                                <th><center>Type</center></th>
                                                <th class="text-center">Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        $i = 1;
                                        $counter = 1;
                                        while ($row = $objQuery->fetch(PDO::FETCH_OBJ)):
                                            $bil = $i + ($PageNo-1)*$PageSize;
                                        
                                            $col1 = $bil.".";
                                            $col2 = $row->nama;
                                            $col3 = $row->type;
                                            $col4 = $row->KodeAddon;
                                        ?>
                                            <?php
                                            echo "<td>$col1</td>";
                                            echo "<td>$col2</td>";
                                            echo "<td>$col3</td>";
                                            echo "<td>";
                                                echo "<a class='btn btn-warning btn-xs' href='../../form/dir/form_form.php?act=addedit&nof=$col4'><i class='fa fa-pencil'></i></a>";
                                                echo "<a class='btn btn-danger btn-xs' href='../../custompage/dir/formulasi.php?exe=add&obj=ele&nof=$col4&strBln=$strBln&strThn=$strThn&PageNo=$PageNo'><i class='fa fa-trash'></i></a>";
                                            echo "</td>";
                                            ?>
                                        <?php
                                        $counter++;
                                        echo "</tr>";
                                        $i++;
                                        endwhile;
                                        ?>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td colspan="8" align="center" height="25">
                                                    <div align="center" class="paging">
                                                        <nav aria-label="page navigation">
                                                            <ul class="pagination">
                                                                <?php
                                                                if($CounterStart != 1):
                                                                    $PrevStart = $CounterStart - 1;
                                                                    print "<li><a href=$_SERVER[SCRIPT_NAME]?PageNo=1&txtKeyword=".$keyword."&strBln=".$strBln."&strThn=".$strThn.">First </a></li>";
                                                                    print "<li><a href=$_SERVER[SCRIPT_NAME]?PageNo=$PrevStart&txtKeyword=".$keyword."&strBln=".$strBln."&strThn=".$strThn.">Previous </a></li>";
                                                                endif;
                                                                $c = 0;
                                                                //print PageNo
                                                                for($c=$CounterStart;$c<=$CounterEnd;$c++):
                                                                    if($c < $MaxPage):
                                                                        if($c == $PageNo):
                                                                            if($c % $PageSize == 0):
                                                                                print "<li class='active'><span>$c<span class='sr-only'></span></span></li>";
                                                                            else:
                                                                                print "<li class='active'><span>$c<span class='sr-only'></span></span></li>";
                                                                            endif;
                                                                        elseif($c % $PageSize == 0):
                                                                            echo "<li><a href=$_SERVER[SCRIPT_NAME]?PageNo=$c&txtKeyword=".$keyword."&chkthn=".$chkthn."&strBln=".$strBln."&strThn=".$strThn.">$c</a></li>";
                                                                        else:
                                                                            echo "<li><a href=$_SERVER[SCRIPT_NAME]?PageNo=$c&txtKeyword=".$keyword."&chkthn=".$chkthn."&strBln=".$strBln."&strThn=".$strThn.">$c</a></li>";
                                                                        endif;
                                                                    else:
                                                                        if($PageNo == $MaxPage):
                                                                            print "<li class='active'><span>$c<span class='sr-only'></span></span></li>";
                                                                            break;
                                                                        else:
                                                                            echo "<li><a href=$_SERVER[SCRIPT_NAME]?PageNo=$c&txtKeyword=".$keyword."&chkthn=".$chkthn."&strBln=".$strBln."&strThn=".$strThn.">$c</a></li>";
                                                                            break;
                                                                        endif;
                                                                    endif;
                                                                endfor;
                                                                if($CounterEnd < $MaxPage):
                                                                    $NextPage = $CounterEnd + 1;
                                                                    echo "<li><a href=$_SERVER[SCRIPT_NAME]?PageNo=$NextPage&txtKeyword=".$keyword."&chkthn=".$chkthn."&strBln=".$strBln."&strThn=".$strThn.">Next</a></li>";
                                                                endif;
                                                                if($CounterEnd < $MaxPage):
                                                                    $LastRec = $RecordCount % $PageSize;
                                                                    if($LastRec == 0):
                                                                        $LajnstartRecord = $RecordCount - $PageSize;
                                                                    else:
                                                                        $LajnstartRecord = $RecordCount - $LastRec;
                                                                    endif;
                                                                    echo "<li><a href=$_SERVER[SCRIPT_NAME]?PageNo=$MaxPage&txtKeyword=".$keyword."&chkthn=".$chkthn."&strBln=".$strBln."&strThn=".$strThn.">Last</a></li>";
                                                                endif;
                                                                ?>
                                                            </ul>
                                                        </nav>
                                                    </div>
                                                </td>                                              
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </form>
                        </div>                        
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>