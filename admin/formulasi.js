/*
 * By dwi.ahmad@icloud.com
 */

$(document).ready(function () {
    $("#nxt").click(function () {
        nof = $("#nof").val();
        nm = $("#nm").val();
        cb = $("#cb").val();
        wr = $("#wr").val();
        $.ajax({
            url: "http://" + location.host + "/project1_5/custompage/dir/formulasi.php?exe=sv&obj=head",
            data: "nof=" + nof + "&nm=" + nm + "&cb=" + cb + "&wr=" + wr,
            cache: false,
            type: 'POST',
            success: function (data) {
                isi = data.split("|");
                if(isi[0] === "0"){
                    $.notify({
                        title: "<strong>Notif: </strong>",
                        message: isi[1]
                    },{
                        delay: 0,
                        type: "success"
                    });
                }else if(isi[0] === "1"){
                    $.notify({
                        title: "<strong>Notif: </strong>",
                        message: isi[1]
                    },{
                        type: "warning"
                    });
                }
            }
        });
        $("#baku").focus();
        $("#form").load("http://" + location.host + "/project1_5/custompage/dir/formulasi.php?nof=" + nof + "&exe=load&obj=dtl");
    });
    $("#baku").keypress(function (e){
       if(e.which === 13){
           brg = $(this).val();
           nof = $("#nof").val();
           $.ajax({
                url: "http://" + location.host + "/project1_5/custompage/dir/formulasi.php?exe=smpn&obj=dtl",
                data: "KodeItem=" + brg + "&nof=" + nof,
                cache: false,
                type: 'POST',
                success: function (data) {
                    rep = data.split("|");
                    if(rep[0] === "0"){
                        $.notify({
                           title: "<strong>Notif: </strong>",
                           message: rep[1]
                        },{
                            type: "warning"
                        });
                    }else if(rep[0] === "1"){
                        $.notify({
                            title: "<strong>Notif :</strong>",
                            message: rep[1]
                        },{
                            type: "success"
                        });
                    }
                }
           });
           $("#form").load("http://" + location.host + "/project1_5/custompage/dir/formulasi.php?nof=" + nof + "&exe=load&obj=dtl");
           $("#baku").val("");
           $("#baku").focus();
       }
    });
    $("#svh").click(function (){
       nof = $("#nof").val();
       nm = $("#nm").val();
       cb = $("#cb").val();
       wr = $("#wr").val();
       $.ajax({
            url: "http://" + location.host + "/project1_5/custompage/dir/formulasi.php?exe=sv&obj=dup",
            data: "nof=" + nof + "&nm=" + nm + "&cb=" + cb + "&wr=" + wr,
            cache: false,
            type: 'POST',
            success: function (data) {
                $.notify({
                   title: "<b>Notif :</b>",
                   message: data
                },{
                    type: "success"
                });
            }
       });
       $("#form").load("http://" + location.host + "/project1_5/custompage/dir/formulasi.php?nof=" + nof + "&exe=load&obj=dtl");
    });
    $("#nxton").click(function (){
       nof = $("#nof").val();
       nm = $("#nm").val();
       tp = $("#tp").val();
       tg = $("#tg").val();
       $.ajax({
          url: "http://" + location.host + "/project1_5/custompage/dir/formulasi.php?exe=hd&obj=on",
          data: "nof=" + nof + "&nm=" + nm + "&tp=" + tp + "&tg=" + tg,
            cache: false,
            type: 'POST',
            success: function (data) {

            }
       });
        $("#formon").load("http://" + location.host + "/project1_5/custompage/dir/formulasi.php?nof=" + nof + "&exe=load&obj=on");
        $("#bakuon").focus();
    });
    $("#bakuon").keypress(function (e){
        if(e.which === 13){
            brg = $(this).val();
           nof = $("#nof").val();
           $.ajax({
                url: "http://" + location.host + "/project1_5/custompage/dir/formulasi.php?exe=dt&obj=on",
                data: "KodeItem=" + brg + "&nof=" + nof,
                cache: false,
                type: 'POST',
                success: function (data) {
                    rep = data.split("|");
                    if(rep[0] === "0"){
                        $.notify({
                           title: "<strong>Notif: </strong>",
                           message: rep[1]
                        },{
                            type: "warning"
                        });
                    }else if(rep[0] === "1"){
                        $.notify({
                            title: "<strong>Notif :</strong>",
                            message: rep[1]
                        },{
                            type: "success"
                        });
                    }
                }
           });
           $("#formon").load("http://" + location.host + "/project1_5/custompage/dir/formulasi.php?nof=" + nof + "&exe=load&obj=on");
           $("#bakuon").val("");
           $("#bakuon").focus();
        }
    });
    $("#trial").click(function (){
       nof = $("#nof").val();
        url = "http://" + location.host + "/project1_5/custompage/dir/formulasi.php?exe=trial&obj=form";
        $.post(url, {nof: nof}, function(trn){
            $(".modal-body").html(trn).show();
        });
    });
    $("#gnt").click(function (){
       var isi = $("#baru").val();
       var lm = $("#awal").val();
       var dt = new Array();
       $("input[name='chk[]']:checked").each(function (){
           dt.push($(this).val());
       });
       $.ajax({
            url: "http://" + location.host + "/project1_5/custompage/dir/formulasi.php?exe=cng&obj=aks",
            type: 'POST',
            data: "kd="+dt+"&isi="+isi+"&awl="+lm,
            cache: false,
            success: function (data) {

            }
       });
       $("#data").load("http://" + location.host + "/project1_5/custompage/dir/formulasi.php?kd=" + lm + "&exe=load&obj=cng");
    });
    $("#upg").click(function (){
        var nof = $("#nof").val(),
            kar = $("#kar").val();
        var src = "http://" + location.host + "/project1_5/custompage/dir/formulasi.php?exe=pop&obj=namjad";
        $.post(src, {nof: nof, kar: kar}, function(trn){
            $(".modal-body").html(trn).show();
        });
    });
    $("#dlk").click(function (){
        var knc = $("#knc").val();
        var nof = $("#nof").val();
        var namjad = $("#namjad").val(),
            kar = $("#kar").val();

       if(knc === "namjad"){
            var src = "http://" + location.host + "/project1_5/custompage/dir/formulasi.php?exe=sv&obj=namjad";
            $.post(src, {nof: nof, namjad: namjad, kar: kar}, function(){
                location.reload();
            });
        }else if(knc === "try"){
            var src = "http://" + location.host + "/project1_5/custompage/dir/formulasi.php?exe=try&obj=inc";
            var arr = new Array();
            $("select[name='xtreme[]']").each(function (){
                arr.push($(this).val());
            });
            var quan = $("#quan").val();
            $.post(src, {isi: arr, nof: nof, quan: quan}, function(dat){
                location.reload();
            });
        }else if(knc === "ktg"){
            var src = "http://" + location.host + "/project1_5/custompage/dir/formulasi.php?exe=sv&obj=ktg";
            var arr = new Array();
            var ray = new Array();
            $("select[name='xtreme[]']").each(function (){
                arr.push($(this).val());
            });
            $("input[name='prktg[]']").each(function (){
                ray.push($(this).val());
            });
            var id = $("#id").val();
            $.post(src, {isi: arr, nof: nof, prktg: ray, id: id}, function(dat){
                $("#form").load("http://" + location.host + "/project1_5/custompage/dir/formulasi.php?nof=" + nof + "&exe=load&obj=dtl");
                $("#pop").modal("hide");
            });
        }
    });
    $("#btnOK").click(function (){
        nof = $("#nof").val();
        $.ajax({
            url: "http://" + location.host + "/project1_5/custompage/dir/formulasi.php?exe=btn&obj=ok",
            type: 'POST',
            data: "nof=" + nof,
            cache: false,
            success: function (data) {
                location.reload();
            }
        });
    });
    $("#btnNO").click(function (){
        nof = $("#nof").val();
        $.ajax({
            url: "http://" + location.host + "/project1_5/custompage/dir/formulasi.php?exe=btn&obj=nok",
            type: 'POST',
            data: "nof=" + nof,
            cache: false,
            success: function (data) {
                location.reload();
            }
        });
    });
    $("#ref").click(function (){
        nof = $("#nof").val();
        $("#form").load("http://" + location.host + "/project1_5/custompage/dir/formulasi.php?nof=" + nof + "&exe=load&obj=dtl");
    });
    $("#refadd").click(function (){
       nof = $("#nof").val();
       $("#formon").load("http://" + location.host + "/project1_5/custompage/dir/formulasi.php?nof=" + nof + "&exe=load&obj=on");
    });
});
$(function (){
   $("#awal").change(function (){
       ki = $("#awal").val();
       $("#data").load("http://" + location.host + "/project1_5/custompage/dir/formulasi.php?kd=" + ki + "&exe=load&obj=cng");
    });
});
function chg(aidi, lue, e){
    if(e.keyCode === 13){
        idia = aidi.split("|");
        fon = idia[0].split("r");
        nof = fon[1];
        ki = idia[1];
        $.ajax({
            url: "http://" + location.host + "/project1_5/custompage/dir/formulasi.php?exe=upd&obj=dtl",
            data: "ki="+ki+"&nof="+nof+"&lue="+lue,
            cache: false,
            type: 'POST',
            success: function (data) {

            }
        });
        $("#form").load("http://" + location.host + "/project1_5/custompage/dir/formulasi.php?nof="+nof+"&exe=load&obj=dtl");
    }
}
function hps(a){
    aa = a.split("|");
    ab = aa[0].split("v");

    $.ajax({
        url: "http://" + location.host + "/project1_5/custompage/dir/formulasi.php?exe=rmv&obj=dtl",
        data: "nof=" + ab[1] + "&ki=" + aa[1],
        cache: false,
        type: 'POST',
        success: function (data) {
            ac = data.split("|");
            if(ac[0] === "0"){
                $.notify({
                    title: "<b>Notif :</b>",
                    message: ac[1]
                },{
                    type: "success"
                });

            }else if(ac[0] === "1"){
                $.notify({
                   title: "<b>Notif :</b>",
                   message: ac[1]
                },{
                    type: "warning"
                });
            }
        }
    });
    $("#form").load("http://" + location.host + "/project1_5/custompage/dir/formulasi.php?nof=" + ab[1] + "&exe=load&obj=dtl");
}
function onchg(aidi, lue, e){
    if(e.keyCode === 13){
        idia = aidi.split("|");
        fon = idia[0].split("r");
        nof = fon[1];
        ki = idia[1];
        $.ajax({
            url: "http://" + location.host + "/project1_5/custompage/dir/formulasi.php?exe=upd&obj=ondtl",
            data: "ki="+ki+"&nof="+nof+"&lue="+lue,
            cache: false,
            type: 'POST',
            success: function (data) {

            }
        });
        $("#formon").load("http://" + location.host + "/project1_5/custompage/dir/formulasi.php?nof="+nof+"&exe=load&obj=on");
    }
}
function onhps(a){
    aa = a.split("|");
    ab = aa[0].split("v");

    $.ajax({
        url: "http://" + location.host + "/project1_5/custompage/dir/formulasi.php?exe=rmv&obj=ondtl",
        data: "nof=" + ab[1] + "&ki=" + aa[1],
        cache: false,
        type: 'POST',
        success: function (data) {
            ac = data.split("|");
            if(ac[0] === "0"){
                $.notify({
                    title: "<b>Notif :</b>",
                    message: ac[1]
                },{
                    type: "success"
                });

            }else if(ac[0] === "1"){
                $.notify({
                   title: "<b>Notif :</b>",
                   message: ac[1]
                },{
                    type: "warning"
                });
            }
        }
    });
    $("#formon").load("http://" + location.host + "/project1_5/custompage/dir/formulasi.php?nof=" + ab[1] + "&exe=load&obj=on");
}
function catt(aidi){
    nof = $("#nof").val();
    $.ajax({
        url: "http://" + location.host + "/project1_5/custompage/dir/formulasi.php?exe=sv&obj=ket",
        data: "nof=" + nof + "&isi=" + aidi,
        cache: false,
        type: 'POST',
        success: function (data) {

        }
    });
}
function knt(aidi, valiu){
    $.ajax({
        url: "http://" + location.host + "/project1_5/custompage/dir/formulasi.php?exe=gnt&obj=knt",
        data: "aidi=" + aidi + "&valiu=" + valiu,
        cache: false,
        type: 'POST',
        success: function (data) {

        }
    });
}
function ktg(dat){
    var nof = $("#nof").val();
    var tad = dat.split("g");
    var noid = tad[1];
    var url = "http://" + location.host + "/project1_5/custompage/dir/formulasi.php?exe=pop&obj=ktg";
    $.post(url, {nof: nof, id: noid}, function(trn){
        $(".modal-body").html(trn).show();
    });
}
