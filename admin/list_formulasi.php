<?php
error_reporting(1);
session_start();

require '../configdb.php';

$mpi = cfg_pdo::connect();
date_default_timezone_set("Asia/Jakarta");

//Status Level
$strLevel = $_SESSION['level'];

//Status Access
$strAccess = $_SESSION['access'];

$username = $_SESSION['username'];

// cek apakah user telah login, jika belum login maka di alihkan ke halaman login
if($_SESSION['status'] !="login"){
	header("location:../index.php");
}

if(isset($_GET['strBln']))
	{$strBln=$_GET["strBln"];}
else
	{$strBln=date('m');}

if(isset($_GET['strThn']))
	{$strThn=$_GET["strThn"];}
else
	{$strThn=date('Y');}
        
?>
<!DOCTYPE html>
<html>
    <head>
	<title>Daftar Formulasi</title>	
        
        <link rel="stylesheet" href="../addon/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="../addon/font-awesome-4.7.0/css/font-awesome.min.css"/>
        <link rel="stylesheet" type="text/css" href="../addon/datatables/dataTables.bootstrap.css">
        <link rel="stylesheet" type="text/css" href="../addon/datatables/dataTables.responsive.css">

        <script type="text/javascript" src="../addon/jquery/jquery-3.2.0.min.js"></script>
        <script src="../addon/bootstrap.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="../addon/datatables/jquery.dataTables.js"></script>
        <script type="text/javascript" src="../addon/datatables/dataTables.responsive.js"></script>
        <script type="text/javascript" src="../addon/datatables/dataTables.bootstrap.js"></script>
        <script type="text/javascript" src="../addon/datatables/common.js"></script>
        <SCRIPT type="text/javascript" src="formulasi.js"></script>
                
        <script type="text/javascript" >

            var dTable;
            // #Example adalah id pada table

            $(document).ready(function() {
                    //Onclick

                dTable = $('#exa').DataTable( {
                    "bProcessing": true,
                    "bServerSide": true,
                    "bJQueryUI": false,
                    "responsive": false,
                    "sAjaxSource": "formulasi_dtl.php?exe=main", // Load Data
                    "sServerMethod": "POST",
                    "deferRender": true,
                    stateSave: true
                } );

            } );

        </script>
                
    </head>
    <?php
	date_default_timezone_set('Asia/Jakarta');
    ?>
    <body style="font-size:12px">
        <div class="container-fluid">
            <div class="row">
                <div><h3><b>Daftar Formulasi</B>&nbsp;&nbsp;<a title="Buat Baru" class="btn btn-info btn-xs" href="form_form.php?act=new&strThn=<?php echo $strThn; ?>&strBln=<?php echo $strBln; ?>"><i class="glyphicon glyphicon-plus"></i></a> <a title="Daftar karet upgrade" class="btn btn-default btn-xs" href="list_Upg.php"><I class="fa fa-list"></I></a></h3></div>
                <p>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <table id="exa" cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered table-condensed table-hover" width="100%" style="font-size:12px">
                            <thead>
                                <tr class="alert-info">
                                    <th width="20%">Nama</th>
                                    <th width="20%">Warna</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                            <tfoot>

                            </tfoot>
                        </table>
                    </div>
                </DIV>
            </div>
        </div>
    </body>
</html>