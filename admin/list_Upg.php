<?php
error_reporting(0);
session_start();
require "../../fungsi.php";
require '../../configdb.php';
login_check_2();
$admin = new adminPage();
$koneksi = cfg_pdo::connect();
$mli = cfg_sqli();
cfg_sql();
date_default_timezone_set("Asia/Jakarta");

//Status Level
$strLevel = $_SESSION['level'];

//Status Access
$strAccess = $_SESSION['access'];

$username = $_SESSION['username2'];

//Mengambil data NIK
$strNIK = $_SESSION['NIK'];
if ($username == ""){
    die("Masa login anda sudah habis, silahkan Login kembali");
}

$admin->beginTransaction();
$admin->query("select bagian, jabatan FROM tbl_kry_departemen WHERE aktif='T' && sts_job='T' && `NIK`='$strNIK'");
$admin->execute();
$bgn = $admin->fetch();
$admin->endTransaction();

$jabatan = $bgn->jabatan;
$bagian = $bgn->bagian;

if(isset($_GET['strBln']))
	{$strBln=$_GET["strBln"];}
else
	{$strBln=date('m');}

if(isset($_GET['strThn']))
	{$strThn=$_GET["strThn"];}
else
	{$strThn=date('Y');}
        
?>
<!DOCTYPE html>
<HTML>
    <HEAD>
	<title>Daftar Formulasi</title>	
        <!-- JQUERY -->
        <script type="text/javascript" src="../../jquery/jquery-1.12.1.min.js"></script>

        <!-- BOOTSTRAP -->
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="../../bootstrap/css/bootstrap.min.css">

        <!-- Latest compiled and minified JavaScript -->
        <script src="../../bootstrap/js/bootstrap.min.js" type="text/javascript"></script>

        <!-- DataTables -->
        <link rel="stylesheet" type="text/css" href="../../add-in/font-awesome-4.7.0/css/font-awesome.min.css"/>
        <link rel="stylesheet" type="text/css" href="../../script/assets/media/css/dataTables.bootstrap.css">
        <link rel="stylesheet" type="text/css" href="../../script/assets/media/css/dataTables.responsive.css">
        <script type="text/javascript" src="../../script/assets/media/js/jquery.dataTables.js"></script>
        <script type="text/javascript" src="../../script/assets/media/js/dataTables.responsive.js"></script>
        <script type="text/javascript" src="../../script/assets/media/js/dataTables.bootstrap.js"></script>
        <script type="text/javascript" src="../../script/assets/media/js/common.js"></script>
        <SCRIPT type="text/javascript" src="../../customjs/dir/formulasi.js"></script>
                
        <script type="text/javascript" >

            var dTable;
            // #Example adalah id pada table

            $(document).ready(function() {
                    //Onclick

                dTable = $('#exa').DataTable( {
                    "bProcessing": true,
                    "bServerSide": true,
                    "bJQueryUI": false,
                    "responsive": false,
                    "sAjaxSource": "upg_dtl.php", // Load Data
                    "sServerMethod": "POST",
                    "deferRender": true,
                    stateSave: true
                } );

            } );

        </script>                
    </HEAD>
    <?php
	date_default_timezone_set('Asia/Jakarta');
    ?>
    <body style="font-size:12px">
        <div class="container-fluid">
            <div class="row">
                <div><h3><b>Daftar Formulasi</B>&nbsp;&nbsp;<a title="Buat Baru" class="btn btn-info btn-xs" href="../../form/dir/form_form.php?act=new&strThn=<?php echo $strThn; ?>&strBln=<?php echo $strBln; ?>"><i class="glyphicon glyphicon-plus"></i></a></h3></div>
                <p>
                <div class="panel panel-default">
                    <DIV class="panel-body">
                        <table id="exa"  cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered table-condensed table-hover" width="100%" style="font-size:12px">
                            <thead>
                                <tr class="alert-info">
                                    <th width="20%">Nama</th>
                                    <th width="20%">Warna</th>
                                    <TH width="30%">Nama Percobaan</TH>
                                    <TH width="10%">% Kerusakan</TH>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                            <tfoot>

                            </tfoot>
                        </table>
                    </DIV>
                </DIV>
            </div>
        </div>
    </body>
</HTML>