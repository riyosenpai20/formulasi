<?php 
if (!empty(getenv("host"))):
    $host = getenv("host");
else:
    $host = "192.168.1.39";
endif;
$usrnm = "matrixp_usrmpi";
$pass = "wer123";
$dbname = "matrix_db";
$port = 3306;

//mysql dengan driver pdo
class cfg_pdo{
    
    public static function connect(){
        global $host, $dbname, $usrnm, $pass;

        try{
            return new PDO('mysql:host='.$host.';dbname='.$dbname.';charset=utf8', $usrnm, $pass);
        }  catch (PDOException $e){
            echo "Connection failed: " . $e->getMessage();
        }
    }
}
?>