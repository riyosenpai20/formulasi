<?php

include 'conf.php';

//untuk koneksi host harus di rubah manual...
class host_pdo{
    public static function connect(){
        try{
            return new PDO('mysql:host=www.matrix-polymer.com;dbname=matrixpo_jkt2;charset=utf8', "matrixpo_usrmpi", "wer040315");
        } catch (PDOException $e){
            echo 'Connection to host failed: '. $e->getMessage();
        }
    }
}

//koneksi ke database mysql dengan driver mysql (deprecated on php 7)
function cfg_sql() {
    global $host, $usrnm, $pass, $dbname;
    
    mysql_connect($host, $usrnm, $pass);
    mysql_set_charset ( 'utf8' );
    mysql_select_db($dbname);
}

//mysql dengan driver mysqli
function cfg_sqli()
{
    global $host, $usrnm, $pass, $dbname, $port;
    
    //Tambah Koneksi lagi
    $gaSql['user']     = $usrnm;
    $gaSql['password'] = $pass;
    $gaSql['db']       = $dbname;  //Database
    $gaSql['server']   = $host;
    $gaSql['port']     = $port; // 3306 is the default MySQL port
    $gaSql['charset']  = 'utf8';

    $db = new mysqli($gaSql['server'], $gaSql['user'], $gaSql['password'], $gaSql['db'], $gaSql['port']);
    if (mysqli_connect_error()) {
            die( 'Error connecting to MySQL server (' . mysqli_connect_errno() .') '. mysqli_connect_error() );
    }

    if (!$db->set_charset($gaSql['charset'])) {
            die( 'Error loading character set "'.$gaSql['charset'].'": '.$db->error );
    }

    return $db;
}

//mysql dengan driver pdo
class cfg_pdo{
    
    public static function connect(){
        global $host, $dbname, $usrnm, $pass;

        try{
            return new PDO('mysql:host='.$host.';dbname='.$dbname.';charset=utf8', $usrnm, $pass);
        }  catch (PDOException $e){
            echo "Connection failed: " . $e->getMessage();
        }
    }
}

// sqlite dengan driver sqlite
class cfg_lite{
    public static function sambung($loc){
        $filename = $loc."dbase/matrix_db.db";
        
        try{
            return new SQLite3($filename);
        } catch (PDOException $e){
            echo "Connection failed: " . $e->getMessage();
        }
    }
}

// sqlite dengan driver pdo
class cfg_ltpdo{
    public static function sambung($loc){
        $filename = $loc."dbase/matrix_db.db";
        
        try{
            return new PDO("sqlite:".$filename);
        } catch (PDOException $e){
            echo "Connection failed: " . $e->getMessage();
        }
    }
}

// mongodb
function cfg_mongo(){
    $mongo = new MongoClient("mongodb://192.168.1.44:27017");
    $db = $mongo->dev_matrix;
    return $db;
}

//untuk koneksi host harus di rubah manual...
class ftmdb{
    public static function connect(){
        try{
            return new PDO('mysql:host=192.168.1.100;dbname=ftm;charset=utf8', "usrbackup", "itmatrix");
        } catch (PDOException $e){
            echo 'Connection to host failed: '. $e->getMessage();
        }
    }
}